{-
hgtkparley -- A vocabulary learning system, tailored to Enligh/Japanese using KVTML
Written by Chris Mennie (chris at chrismennie.ca)
Copyright (C) 2015 Chris A. Mennie

License: Released under the GPL version 3 license. See the included LICENSE.
-}

module Main (main) where

import Options
import qualified Graphics.UI.Gtk as Gtk
import Prelude (String, IO, ($), (==), (.), print)
import  Data.List hiding (
    all, and, any, concat, concatMap, find, foldl,
    foldl', foldl1, foldr, foldr1, mapAccumL,
    mapAccumR, maximum, maximumBy, minimum,
    minimumBy ,notElem ,or ,product ,sum)
import Control.Applicative
import Data.Text hiding (last)
import Data.Bool
import Data.Int
import Data.IORef
import Data.Time.LocalTime
import Control.Monad
import qualified System.Process as SP

import UI.Types
import UI.GtkParleyWindow
import UI.PracticeWindow
import UI.EditWindow
import Kvtml.Kvtml
import Quiz
import Preferences.Preferences


data MainOptions = MainOptions
    {
        preferencesFolder :: String
    }

instance Options MainOptions where
    defineOptions = pure MainOptions
        <*> simpleOption "preferencesFolder" "./" "The preferences folder."


main :: IO ()
main = runCommand $ \opts _ -> do
    let filename = "Kanji in MangaLand.kvtml"
    let prefsFolderBase = preferencesFolder opts
    let prefsFolder = if last prefsFolderBase == '/'
                        then prefsFolderBase
                        else prefsFolderBase ++ "/"

    let preferencesFile = pack $ prefsFolder ++ "Preferences.xml"
    let fullFilename = pack $ prefsFolder ++ filename


    -- Load data
    enabledLessons <- loadSelectedFile (pack $ prefsFolder ++ filename ++ ".selected.xml")

    quizData <- loadKvtmlFile (pack $ prefsFolder ++ filename)
    quizDataRef <- newIORef quizData

    preferences <- loadPreferencesFile preferencesFile
    preferencesRef <- newIORef preferences

    -- Start UI bits
    _ <- Gtk.initGUI

    builder <- Gtk.builderNew
    Gtk.builderAddFromFile builder "GtkParley.glade"

    practicewindow <- initPracticeWindow builder
                        (updateAndSaveQuizData quizDataRef (pack prefsFolder) fullFilename)
                        (updatePracticeWindowPosSize preferencesRef) preferences

    editWindow <- initEditWindow builder quizDataRef
                        (saveQuizData quizDataRef (pack prefsFolder) fullFilename)
                        preferences

    mainwindow <- initMainWidow builder quizDataRef practicewindow editWindow
                        (updateMainWindowPosSize preferencesRef) preferences

    gpw_updateLessonTree mainwindow quizData
    gpw_updateUIWithEnabledLessons mainwindow enabledLessons

    Gtk.widgetShowAll $ gpw_gtkwindow mainwindow
    Gtk.mainGUI

    -- Save final state
    gpw_saveEnabledStates mainwindow (pack $ prefsFolder ++ filename ++ ".selected.xml")

    finalPrefs <- readIORef preferencesRef
    savePreferencesFile finalPrefs preferencesFile

    -- Save final quiz state (probably hasn't changed)
    saveQuizData quizDataRef (pack prefsFolder) fullFilename


saveQuizData :: IORef Kvtml -> Text -> Text -> IO ()
saveQuizData quizDataRef prefsFolder fullFilename = do
    currentQuiz <- readIORef quizDataRef
    saveKvtmlFile currentQuiz fullFilename
    commitChanges prefsFolder


updateAndSaveQuizData :: IORef Kvtml -> Text -> Text -> Bool -> QuizResults -> IO ()
updateAndSaveQuizData quizDataRef prefsFolder filename isEJ results = do
    localTime <- getZonedTime
    let currentDate = (localDay . zonedTimeToLocalTime) localTime

    print currentDate

    currentQuiz <- readIORef quizDataRef
    let updatedQuiz = updateQuizWithResults currentQuiz results isEJ currentDate

    writeIORef quizDataRef updatedQuiz
    saveKvtmlFile updatedQuiz filename

    commitChanges prefsFolder


commitChanges :: Text -> IO ()
commitChanges prefsFolder = do
    let cmd = "bash -c \"cd " `append` prefsFolder `append` "; hg commit -m 'GtkParley updates'\"";

    _ <- SP.system $ unpack cmd
    return ()


updatePracticeWindowPosSize :: IORef Preferences -> Int -> Int -> Int -> Int -> IO ()
updatePracticeWindowPosSize preferencesRef width height x y = do
    oldPrefs <- readIORef preferencesRef

    let newPrefs = oldPrefs
                        {
                            pr_practiceWindowWidth = width,
                            pr_practiceWindowHeight = height,
                            pr_practiceWindowX = x,
                            pr_practiceWindowY = y
                        }

    writeIORef preferencesRef newPrefs


updateMainWindowPosSize :: IORef Preferences -> Int -> Int -> Int -> Int -> IO ()
updateMainWindowPosSize preferencesRef width height x y = do
    oldPrefs <- readIORef preferencesRef

    let newPrefs = oldPrefs
                        {
                            pr_mainWindowWidth = width,
                            pr_mainWindowHeight = height,
                            pr_mainWindowX = x,
                            pr_mainWindowY = y
                        }

    writeIORef preferencesRef newPrefs
