{-
hgtkparley -- A vocabulary learning system, tailored to Enligh/Japanese using KVTML
Written by Chris Mennie (chris at chrismennie.ca)
Copyright (C) 2015 Chris A. Mennie

License: Released under the GPL version 3 license. See the included LICENSE.
-}

module Quiz (
    getDueEntries,
    updateQuizWithResults,
    entryGrade
)

where

import UI.Types
import Kvtml.Kvtml

import qualified Data.HashMap.Strict as HM
import Data.Bool
import Data.Int
import Prelude ((<), (>), (-), ($), (+), (.), min, fromInteger)
import  Data.List hiding (
    all, and, any, concat, concatMap, find, foldl,
    foldl', foldl1, foldr, foldr1, mapAccumL,
    mapAccumR, maximum, maximumBy, minimum,
    minimumBy ,notElem ,or ,product ,sum )
import Data.Foldable
import Data.Maybe
import Data.Time.Calendar
import qualified Data.Vector as V
import GHC.Exts (groupWith)


decayRates :: V.Vector Int
decayRates = V.fromList [-1, 1, 2, 4, 7, 14, 30, 45, 60, 75, 90, 105]


entryGrade :: Bool -> KvtmlEntry -> Int
entryGrade isEJ entry =
        grade
    where
        translationM = HM.lookup (translationLookupIndex isEJ) (kvtmlEntry_translations entry)

        translationGrade = maybe 0
                            (kvtmlGrade_currentGrade . kvtmlTrans_Grade)
                            translationM

        grade
            | translationGrade < 0 = 0
            | translationGrade > V.length decayRates - 1 = V.length decayRates - 1
            | otherwise = translationGrade


isDueEntry :: Bool -> Day -> KvtmlEntry -> Bool
isDueEntry isEJ currentDate entry =
        diffD > decayRate
    where
        translationM = HM.lookup (translationLookupIndex isEJ) (kvtmlEntry_translations entry)

        translationDate =
            maybe currentDate
                (kvtmlGrade_date . kvtmlTrans_Grade)
                translationM

        diffD = fromInteger $ diffDays currentDate translationDate
        grade = entryGrade isEJ entry

        decayRate = fromMaybe (-1) (decayRates V.!? grade)


getDueEntries :: Kvtml -> Bool -> [KvtmlLesson] -> Day -> [[KvtmlEntry]]
getDueEntries kvtml isEJ activeEnabledLessons currentDate =
        dueEntriesGroupedByGrade
    where
        allEntryIds = concatMap kvtmlLesson_entries activeEnabledLessons
        allEntries = kvtmlEntryIdsToEntry kvtml allEntryIds
        dueEntries = filter (isDueEntry isEJ currentDate) allEntries
        eGrade = entryGrade isEJ
        dueEntriesGroupedByGrade = groupWith eGrade dueEntries


updateQuizWithResults :: Kvtml -> QuizResults -> Bool -> Day -> Kvtml
updateQuizWithResults kvtml results isEJ today =
        kvtml { kvtml_entries = updatedEntries }
    where
        updatedEntries = HM.foldrWithKey updateEntryMapWithResult (kvtml_entries kvtml) results

        updateEntryMapWithResult entryId didPass entryMap =
            let entryM = HM.lookup entryId entryMap in
            case entryM of
                Nothing -> entryMap -- Note: Very bad, shouldn't be possible
                Just entry ->
                    let updatedEntry = updateEntryGrade entry didPass isEJ today in
                    HM.insert entryId updatedEntry entryMap


updateEntryGrade :: KvtmlEntry -> Bool -> Bool -> Day -> KvtmlEntry
updateEntryGrade entry didPass isEJ today =
    case translationM of
        Nothing -> entry
        Just translation ->
            let updatedTranslation = updateEntryTranslationGrade didPass today translation in
            let updatedTranslationMap = HM.insert (translationLookupIndex isEJ) updatedTranslation (kvtmlEntry_translations entry) in
            entry { kvtmlEntry_translations = updatedTranslationMap }

    where
        translationM = HM.lookup (translationLookupIndex isEJ) (kvtmlEntry_translations entry)


updateEntryTranslationGrade :: Bool -> Day -> KvtmlTranslation -> KvtmlTranslation
updateEntryTranslationGrade didPass today translation =
        translation { kvtmlTrans_Grade = updatedGrade }
    where
        translationGrade = kvtmlTrans_Grade translation

        newGrade = if didPass
                        then min (kvtmlGrade_currentGrade translationGrade + 1) (V.length decayRates - 1)
                        else 0

        updatedGrade = KvtmlGrade
            {
                kvtmlGrade_currentGrade = newGrade,
                kvtmlGrade_count = kvtmlGrade_count translationGrade + 1,
                kvtmlGrade_errorCount = if didPass
                                            then kvtmlGrade_errorCount translationGrade
                                            else kvtmlGrade_errorCount translationGrade + 1
                ,kvtmlGrade_date = today
            }
