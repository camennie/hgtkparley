{-
hgtkparley -- A vocabulary learning system, tailored to Enligh/Japanese using KVTML
Written by Chris Mennie (chris at chrismennie.ca)
Copyright (C) 2015 Chris A. Mennie

License: Released under the GPL version 3 license. See the included LICENSE.
-}

module Kvtml.Types

where

import Data.Text
import Data.Time
import Data.Int
import Data.HashMap.Strict
import Data.Bool
import Data.Ord
import Data.Functor
import Data.Eq
import qualified Data.HashMap.Strict as HM
import Prelude (Show, (++), show, (.), ($))


type KvtmlIdentifiers = HashMap Int KvtmlIdentifier
type KvtmlEntries = HashMap Int KvtmlEntry
type KvtmlTranslations = HashMap Int KvtmlTranslation
type SelectedFileData = HM.HashMap Text Bool


data Kvtml = Kvtml
    {
        kvtml_information :: KvtmlInformation,
        kvtml_identifiers :: KvtmlIdentifiers,
        kvtml_entries :: KvtmlEntries,
        kvtml_lessons :: [KvtmlLesson]
    } deriving(Show)


defaultKvtml :: Kvtml
defaultKvtml = Kvtml
    {
        kvtml_information = defaultKvtmlInformation,
        kvtml_identifiers = Data.HashMap.Strict.empty,
        kvtml_entries = Data.HashMap.Strict.empty,
        kvtml_lessons = []
    }


data KvtmlInformation = KvtmlInformation
    {
        kvtmlInfo_generator :: Text,
        kvtmlInfo_title :: Text,
        kvtmlInfo_author :: Text,
        kvtmlInfo_contact :: Text,
        kvtmlInfo_license :: Text,
        kvtmlInfo_date :: Day,
        kvtmlInfo_category :: Text
    } deriving(Show, Eq, Ord)


defaultKvtmlDate :: Day
defaultKvtmlDate = fromGregorian 2015 1 1


defaultKvtmlInformation :: KvtmlInformation
defaultKvtmlInformation = KvtmlInformation
    {
        kvtmlInfo_generator = "",
        kvtmlInfo_title = "",
        kvtmlInfo_author = "",
        kvtmlInfo_contact = "",
        kvtmlInfo_license = "",
        kvtmlInfo_date = defaultKvtmlDate,
        kvtmlInfo_category = ""
    }


data KvtmlIdentifier = KvtmlIdentifier
    {
        kvtmlIdent_id :: Int,
        kvtmlIdent_name :: Text,
        kvtmlIdent_locale :: Text
    } deriving(Show, Eq, Ord)


data KvtmlEntry = KvtmlEntry
    {
        kvtmlEntry_id :: Int,
        kvtmlEntry_translations :: KvtmlTranslations
    } deriving(Show, Eq)

instance Ord KvtmlEntry where
    (KvtmlEntry id1 _) `compare` (KvtmlEntry id2 _) = id1 `compare` id2


defaultKvtmlEntry :: KvtmlEntry
defaultKvtmlEntry = KvtmlEntry
                        {
                            kvtmlEntry_id = -1,
                            kvtmlEntry_translations = HM.empty
                        }


data KvtmlTranslation = KvtmlTranslation
    {
        kvtmlTrans_id :: Int,
        kvtmlTrans_text :: Text,
        kvtmlTrans_fullText :: Text,
        kvtmlTrans_Grade :: KvtmlGrade
    } deriving(Show, Eq, Ord)


defaultKvtmlTranslation :: Int -> KvtmlTranslation
defaultKvtmlTranslation i = KvtmlTranslation
                            {
                                kvtmlTrans_id = i,
                                kvtmlTrans_text = "",
                                kvtmlTrans_fullText = "",
                                kvtmlTrans_Grade = defaultKvtmlGrade
                            }


data KvtmlGrade = KvtmlGrade
    {
        kvtmlGrade_currentGrade :: Int,
        kvtmlGrade_count :: Int,
        kvtmlGrade_errorCount :: Int,
        kvtmlGrade_date :: Day
    } deriving(Show, Eq, Ord)


defaultKvtmlGrade :: KvtmlGrade
defaultKvtmlGrade = KvtmlGrade
    {
        kvtmlGrade_currentGrade = 0,
        kvtmlGrade_count = 0,
        kvtmlGrade_errorCount = 0,
        kvtmlGrade_date = defaultKvtmlDate
    }


data KvtmlLesson = KvtmlLesson
    {
        kvtmlLesson_parent :: Text,
        kvtmlLesson_name :: Text,
        kvtmlLesson_lessons :: [KvtmlLesson],
        kvtmlLesson_entries :: [Int]
    }


kvtmlLesson_fullName :: KvtmlLesson -> Text
kvtmlLesson_fullName lesson =
        case parentName of
            "" -> kvtmlLesson_name lesson
            _ -> parentName `append` "/" `append` kvtmlLesson_name lesson
    where
        parentName = kvtmlLesson_parent lesson


instance Show KvtmlLesson where
    show (KvtmlLesson parent name lessons entries) =
            "{KvtmlLesson name='" ++ unpack name ++ "' parent='"
            ++ unpack parent ++ "' entries: "
            ++ entryIdsStr ++ "\n childLessons: ["
            ++ show lessons ++ "]}\n"
        where
            entryIds = entries
            entryIdsStr = unpack $ intercalate ", " (fmap (pack.show) entryIds)
