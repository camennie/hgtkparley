{-
hgtkparley -- A vocabulary learning system, tailored to Enligh/Japanese using KVTML
Written by Chris Mennie (chris at chrismennie.ca)
Copyright (C) 2015 Chris A. Mennie

License: Released under the GPL version 3 license. See the included LICENSE.
-}

module Kvtml.Import (
    loadKvtmlFile,
    loadSelectedFile
)

where

import  Data.List hiding (
    all, and, any, concat, concatMap, find, foldl,
    foldl', foldl1, foldr, foldr1, mapAccumL,
    mapAccumR, maximum, maximumBy, minimum,
    minimumBy ,notElem ,or ,product ,sum )
import Control.Monad hiding (forM , forM_ , mapM , mapM_ , msum , sequence , sequence_ )
import Data.Traversable
import Kvtml.Types
import Text.XML
import Data.Maybe
import Data.Bool
import Data.Text hiding (zip, map, filter, find)
import Data.Int
import qualified Data.Map.Lazy as M
import qualified Data.HashMap.Strict as HM
import Prelude (IO, (==), ($), (.))
import Safe
import XMLHelpers


loadKvtmlFile :: Text -> IO Kvtml
loadKvtmlFile fileName = do
    Document _ root _ <- readFile def (unpack fileName)
    return $ fromMaybe defaultKvtml $ loadKvtml root


loadKvtml :: Element -> Maybe Kvtml
loadKvtml root = do
        kvtmlIdentifiers <- parseKvtmlIdentifiers root
        kvtmlEntries <- parseKvtmlEntries root
        kvtmlLessons <- parseKvtmlLessons root kvtmlEntries

        return defaultKvtml
                {
                    kvtml_information = kvtml_info,
                    kvtml_identifiers = kvtmlIdentifiers,
                    kvtml_entries = kvtmlEntries,
                    kvtml_lessons = kvtmlLessons
                }
    where
        kvtml_info = parseKvtmlInformation root


parseIdentifier :: Element -> Maybe KvtmlIdentifier
parseIdentifier identifierElement = do
    idVal <- M.lookup "id" (elementAttributes identifierElement) >>= readMay . unpack
    name <- getNamedChildNodeM "name" identifierElement >>= getElementTextNodeM
    locale <- getNamedChildNodeM "locale" identifierElement >>= getElementTextNodeM

    return KvtmlIdentifier
            {
                kvtmlIdent_id = idVal,
                kvtmlIdent_name = name,
                kvtmlIdent_locale = locale
            }


parseKvtmlIdentifiers :: Element -> Maybe KvtmlIdentifiers
parseKvtmlIdentifiers root = do
    identifierNodes <- getNamedChildNodeM "identifiers" root >>=
                       getNamedChildNodesM "identifier"

    identifiers <- mapM parseIdentifier identifierNodes

    let kvPairs = zip (fmap kvtmlIdent_id identifiers) identifiers

    return $ HM.fromList kvPairs


parseTranslation :: Element -> Maybe KvtmlTranslation
parseTranslation translationElement = do
    idVal <- M.lookup "id" (elementAttributes translationElement) >>= readMay . unpack

    text <- getNamedChildNodeM "text" translationElement >>=
            getElementTextNodeM

    let fullText = case getNamedChildNodeM "full_text" translationElement of
                        Nothing -> ""
                        Just element -> getOptionalElementTextNodeM element

    let gradeNodeM = getNamedChildNodeM "grade" translationElement

    case gradeNodeM of
        Nothing ->
            return KvtmlTranslation
                        {
                            kvtmlTrans_id = idVal,
                            kvtmlTrans_text = text,
                            kvtmlTrans_fullText = fullText,
                            kvtmlTrans_Grade = defaultKvtmlGrade
                        }
        Just gradeNode -> do

            currentGrade <- getNamedChildNodeM "currentgrade" gradeNode >>=
                            getElementTextNodeM >>=
                            readMay . unpack

            tcount <- getNamedChildNodeM "count" gradeNode >>=
                      getElementTextNodeM >>=
                      readMay . unpack

            errorcount <- getNamedChildNodeM "errorcount" gradeNode >>=
                          getElementTextNodeM >>=
                          readMay . unpack

            date <- getNamedChildNodeM "date" gradeNode >>=
                    getElementTextNodeM >>=
                    parseDayM

            return KvtmlTranslation
                       {
                           kvtmlTrans_id = idVal,
                           kvtmlTrans_text = text,
                           kvtmlTrans_fullText = fullText,
                           kvtmlTrans_Grade = KvtmlGrade
                                                  {
                                                      kvtmlGrade_currentGrade = currentGrade,
                                                      kvtmlGrade_count = tcount,
                                                      kvtmlGrade_errorCount = errorcount,
                                                      kvtmlGrade_date = date
                                                  }
                       }


parseEntry :: Element -> Maybe KvtmlEntry
parseEntry entryNode = do
    idVal <- M.lookup "id" (elementAttributes entryNode) >>= readMay . unpack

    translations <- getNamedChildNodesM "translation" entryNode >>= mapM parseTranslation

    let kvPairs = zip (fmap kvtmlTrans_id translations) translations

    return KvtmlEntry
               {
                   kvtmlEntry_id = idVal,
                   kvtmlEntry_translations = HM.fromList kvPairs
               }


parseKvtmlEntries :: Element -> Maybe KvtmlEntries
parseKvtmlEntries root = do
    entryNodes <- getNamedChildNodeM "entries" root >>=
                  getNamedChildNodesM "entry"

    entries <- mapM parseEntry entryNodes

    let kvPairs = zip (fmap kvtmlEntry_id entries) entries

    return $ HM.fromList kvPairs


parseLesson :: Text -> KvtmlEntries -> Element -> Maybe KvtmlLesson
parseLesson parentName entryMap lessonNode = do
    name <- getNamedChildNodeM "name" lessonNode >>=
            getElementTextNodeM

    let childParentName = if Data.Text.null parentName
                            then name
                            else parentName `append` "/" `append` name

    childLessons <- getNamedChildNodesM "container" lessonNode >>= mapM (parseLesson childParentName entryMap)

    lessonEntryIds <- getNamedChildNodesM "entry" lessonNode >>=
                      mapM (\n -> M.lookup "id" (elementAttributes n) >>= readMay . unpack)

    return KvtmlLesson
               {
                   kvtmlLesson_parent = parentName,
                   kvtmlLesson_name = name,
                   kvtmlLesson_lessons = childLessons,
                   kvtmlLesson_entries = lessonEntryIds
               }


parseKvtmlLessons :: Element -> KvtmlEntries -> Maybe [KvtmlLesson]
parseKvtmlLessons root kvtmlEntries = do
    topLevelLessonNodes <- getNamedChildNodeM "lessons" root >>=
                           getNamedChildNodesM "container"

    mapM (parseLesson "" kvtmlEntries) topLevelLessonNodes


parseKvtmlInformation :: Element -> KvtmlInformation
parseKvtmlInformation rootElem =
        maybe defaultKvtmlInformation contructKvtmlInformation $ getNamedChildNodeM "information" rootElem
    where
        contructKvtmlInformation informationNode =
                KvtmlInformation {
                    kvtmlInfo_generator = generator,
                    kvtmlInfo_title = title,
                    kvtmlInfo_author = author,
                    kvtmlInfo_contact = contact,
                    kvtmlInfo_license = license,
                    kvtmlInfo_date = date,
                    kvtmlInfo_category = category
                }
            where
                generator = fromMaybe "" $ getNamedChildNodeTextM "generator" informationNode
                title = fromMaybe "" $ getNamedChildNodeTextM "title" informationNode
                dateStr = fromMaybe "" $ getNamedChildNodeTextM "date" informationNode
                author = fromMaybe "" $ getNamedChildNodeTextM "author" informationNode
                contact = fromMaybe "" $ getNamedChildNodeTextM "contact" informationNode
                license = fromMaybe "" $ getNamedChildNodeTextM "license" informationNode
                category = fromMaybe "" $ getNamedChildNodeTextM "category" informationNode

                date = parseDay dateStr


loadSelectedFile :: Text -> IO SelectedFileData
loadSelectedFile fileName = do
    Document _ root _ <- readFile def (unpack fileName)
    return $ fromMaybe HM.empty $ loadSelectedFileNode root


loadSelectedFileNode :: Element -> Maybe SelectedFileData
loadSelectedFileNode root = do
    selectedNodes <- getNamedChildNodesM "Selected" root

    kvPairs <- mapM parseSelectedNode selectedNodes

    return $ HM.fromList kvPairs


parseSelectedNode :: Element -> Maybe (Text, Bool)
parseSelectedNode selectedNode = do
    name <- M.lookup "name" (elementAttributes selectedNode)
    enabledInt <- M.lookup "enabled" (elementAttributes selectedNode) >>= readMay . unpack :: Maybe Int
    let enabled = enabledInt == 1

    return (name, enabled)
