{-
hgtkparley -- A vocabulary learning system, tailored to Enligh/Japanese using KVTML
Written by Chris Mennie (chris at chrismennie.ca)
Copyright (C) 2015 Chris A. Mennie

License: Released under the GPL version 3 license. See the included LICENSE.
-}

module Kvtml.Kvtml (
    module Kvtml,

    kvtmlEntryIdToEntry,
    kvtmlEntryIdsToEntry,

    kvtmlGetMaxEntryId,
    addNewChildLesson,
    addNewQuizEntry
)

where

import Kvtml.Types as Kvtml
import Kvtml.Import as Kvtml
import Kvtml.Export as Kvtml

import qualified Data.HashMap.Strict as HM
import Data.Int
import Data.Maybe
import Data.Functor
import Data.Text
import Data.List ((++))
import Prelude ((==), otherwise)
import Data.Ord


kvtmlEntryIdToEntry :: Kvtml -> Int -> KvtmlEntry
kvtmlEntryIdToEntry kvtml entryId =
        fromMaybe defaultKvtmlEntry entryM
    where
        entryM = HM.lookup entryId (kvtml_entries kvtml)


kvtmlEntryIdsToEntry :: Kvtml -> [Int] -> [KvtmlEntry]
kvtmlEntryIdsToEntry kvtml = fmap (kvtmlEntryIdToEntry kvtml)


kvtmlGetMaxEntryId :: Kvtml -> Int
kvtmlGetMaxEntryId kvtml =
    HM.foldr (\e m -> max m (kvtmlEntry_id e)) 0 (kvtml_entries kvtml)


addNewQuizEntry :: Kvtml -> Text -> Text -> Text -> Int -> [Text] -> Kvtml
addNewQuizEntry kvtml japaneseText fullText englishText itemId parentLessonPath =
        kvtml
            {
                kvtml_entries = HM.insert itemId newEntry (kvtml_entries kvtml),
                kvtml_lessons = fmap (addLessonEntry parentLessonPath itemId) (kvtml_lessons kvtml)
            }
    where
        newEntry = KvtmlEntry
                    {
                        kvtmlEntry_id = itemId,
                        kvtmlEntry_translations =
                            HM.fromList [(kvtmlTrans_id japaneseTranslation, japaneseTranslation),
                                         (kvtmlTrans_id englishTranslation, englishTranslation)]
                    }
        japaneseTranslation = (defaultKvtmlTranslation 0) { kvtmlTrans_text = japaneseText, kvtmlTrans_fullText = fullText }
        englishTranslation = (defaultKvtmlTranslation 1) { kvtmlTrans_text = englishText }

        addLessonEntry :: [Text] -> Int -> KvtmlLesson -> KvtmlLesson
        addLessonEntry [curPathName] entryId curLesson
            | compare (kvtmlLesson_name curLesson) curPathName == EQ =
                            curLesson
                                {
                                    kvtmlLesson_entries = kvtmlLesson_entries curLesson ++ [entryId]
                                }
            | otherwise = curLesson

        addLessonEntry (curPathName:remainingPathNames) entryId curLesson
            | compare (kvtmlLesson_name curLesson) curPathName == EQ =
                curLesson
                    {
                        kvtmlLesson_lessons =
                            fmap (addLessonEntry remainingPathNames entryId)
                                 (kvtmlLesson_lessons curLesson)
                    }
            | otherwise = curLesson

        addLessonEntry [] _ curLesson = curLesson -- Note: This should never happen


addNewChildLesson :: Kvtml -> [Text] -> KvtmlLesson -> Kvtml
addNewChildLesson kvtml parentLessonPath childLesson =
        kvtml
            {
                kvtml_lessons = fmap (addChildLesson parentLessonPath childLesson) (kvtml_lessons kvtml)
            }
    where
        addChildLesson :: [Text] -> KvtmlLesson -> KvtmlLesson -> KvtmlLesson
        addChildLesson [curPathName] child curLesson
            | compare (kvtmlLesson_name curLesson) curPathName == EQ =
                            curLesson
                                {
                                    kvtmlLesson_lessons = kvtmlLesson_lessons curLesson ++ [child]
                                }
            | otherwise = curLesson

        addChildLesson (curPathName:remainingPathNames) child curLesson
            | compare (kvtmlLesson_name curLesson) curPathName == EQ =
                curLesson
                    {
                        kvtmlLesson_lessons =
                            fmap (addChildLesson remainingPathNames child)
                                 (kvtmlLesson_lessons curLesson)
                    }
            | otherwise = curLesson

        addChildLesson [] _ curLesson = curLesson -- Note: This should never happen
