{-
hgtkparley -- A vocabulary learning system, tailored to Enligh/Japanese using KVTML
Written by Chris Mennie (chris at chrismennie.ca)
Copyright (C) 2015 Chris A. Mennie

License: Released under the GPL version 3 license. See the included LICENSE.
-}

module Kvtml.Export (
    saveKvtmlFile,
    saveSelectedFile
)

where

import  Data.List hiding (
    all, and, any, concat, concatMap, find, foldl,
    foldl', foldl1, foldr, foldr1, mapAccumL,
    mapAccumR, maximum, maximumBy, minimum,
    minimumBy ,notElem ,or ,product ,sum )
import Kvtml.Types
import Text.XML
import Data.Maybe
import Data.Time
import Data.Bool
import qualified Data.Text as T
import qualified Data.Text.Lazy as L
import Data.Int
import qualified Data.Text.Lazy.IO as DTIO
import qualified Data.Map.Lazy as M
import qualified Data.HashMap.Strict as HM
import Prelude (IO, ($), (+), snd, show, (==))



saveSelectedFile :: SelectedFileData -> T.Text -> IO ()
saveSelectedFile selectedFileData filename =
        writeFile (def { rsPretty = True }) (T.unpack filename) xmlDoc
    where
        kvPairs = HM.toList selectedFileData
        sortedKVPairs = sort kvPairs

        elements = map toElem sortedKVPairs

        xmlDoc = Document (Prologue [] Nothing []) rootNode []
        rootNode = Element "kvtml_selected" M.empty elements

        toElem (k, v) = NodeElement $ Element "Selected" (M.fromList [("name", k), ("enabled", boolStr v)]) []
        boolStr b = if b then "1" else "0"


-- XXX: xml-conduit screws up text nodes when pretty-printing, so we do this by hand.. gross.
saveKvtmlFile :: Kvtml -> T.Text -> IO ()
saveKvtmlFile kvtml filename =
        DTIO.writeFile (T.unpack filename) kvtmlStr
    where
        kvtmlStr = kvtmlXMLString kvtml


kvtmlXMLString :: Kvtml -> L.Text
kvtmlXMLString kvtml =
        xmlStr `L.append`
        doctype `L.append`
        kvtml_st `L.append`
          getInformationNode kvtml `L.append`
          getIdentifiersNode kvtml `L.append`
          getEntriesNode kvtml `L.append`
          getLessonsNode kvtml `L.append`
        kvtml_ed
    where
        xmlStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
        doctype = "<!DOCTYPE kvtml PUBLIC \"kvtml2.dtd\" \"http://edu.kde.org/kvtml/kvtml2.dtd\">\n"
        kvtml_st = "<kvtml version=\"2.0\">\n"
        kvtml_ed = "</kvtml>"


getInformationNode :: Kvtml -> L.Text
getInformationNode kvtml =
        "  <information>\n" `L.append`
        "    <generator>hgtkparley 1.0</generator>\n" `L.append`
        "    <title>" `L.append` title `L.append` "</title>\n" `L.append`
        "    <author>" `L.append` author `L.append` "</author>\n" `L.append`
        "    <contact>" `L.append` contact `L.append` "</contact>\n" `L.append`
        "    <license>" `L.append` license `L.append` "</license>\n" `L.append`
        "    <date>" `L.append` formattedDate `L.append` "</date>\n" `L.append`
        "    <category>" `L.append` category `L.append` "</category>\n" `L.append`
        "  </information>\n"
    where
        information = kvtml_information kvtml

        -- generator = L.fromStrict $ kvtmlInfo_generator information
        title = L.fromStrict $ kvtmlInfo_title information
        author = L.fromStrict $ kvtmlInfo_author information
        contact = L.fromStrict $ kvtmlInfo_contact information
        license = L.fromStrict $ kvtmlInfo_license information
        formattedDate = L.pack $ formatTime defaultTimeLocale "%Y-%m-%d" (kvtmlInfo_date information)
        category = L.fromStrict $ kvtmlInfo_category information


getIdentifiersNode :: Kvtml -> L.Text
getIdentifiersNode kvtml =
        "  <identifiers>\n" `L.append`
            L.concat identifierXmls `L.append`
        "  </identifiers>\n"
    where
        identifiers = map snd (sort (HM.toList $ kvtml_identifiers kvtml))
        identifierXmls = map getIdentifierXml identifiers


getIdentifierXml :: KvtmlIdentifier -> L.Text
getIdentifierXml identifier =
        "    <identifier id=\"" `L.append` id `L.append` "\">\n" `L.append`
        "      <name>" `L.append` name `L.append` "</name>\n" `L.append`
        "      <locale>" `L.append` locale `L.append` "</locale>\n" `L.append`
        "    </identifier>\n"
    where
        id = L.pack (show $ kvtmlIdent_id identifier)
        name = L.fromStrict $ kvtmlIdent_name identifier
        locale = L.fromStrict $ kvtmlIdent_locale identifier


getEntriesNode :: Kvtml -> L.Text
getEntriesNode kvtml =
        "  <entries>\n" `L.append`
            L.concat entryXmls `L.append`
        "  </entries>\n"
    where
        entries = map snd (sort (HM.toList $ kvtml_entries kvtml))
        entryXmls = map getEntryXml entries


getEntryXml :: KvtmlEntry -> L.Text
getEntryXml entry =
        "    <entry id=\"" `L.append` id `L.append` "\">\n" `L.append`
            L.concat translationXmls `L.append`
        "    </entry>\n"
    where
        id = L.pack (show $ kvtmlEntry_id entry)
        translations = map snd (sort (HM.toList $ kvtmlEntry_translations entry))
        translationXmls = map getTranslationXml translations


getTranslationXml :: KvtmlTranslation -> L.Text
getTranslationXml translation =
            "      <translation id=\"" `L.append` id `L.append` "\">\n" `L.append`
            "        <text>" `L.append` text `L.append` "</text>\n" `L.append`
            fullTextStr `L.append`
            "        <grade>\n" `L.append`
            "          <currentgrade>" `L.append` currentgrade `L.append` "</currentgrade>\n" `L.append`
            "          <count>" `L.append` count `L.append` "</count>\n" `L.append`
            "          <errorcount>" `L.append` errorcount `L.append` "</errorcount>\n" `L.append`
            "          <date>" `L.append` date `L.append` "</date>\n" `L.append`
            "        </grade>\n" `L.append`
            "      </translation>\n"
        where
            id = L.pack (show $ kvtmlTrans_id translation)
            text = L.fromStrict $ kvtmlTrans_text translation
            fullText = L.fromStrict $ kvtmlTrans_fullText translation
            grade = kvtmlTrans_Grade translation
            currentgrade = L.pack (show $ kvtmlGrade_currentGrade grade)
            count = L.pack (show $ kvtmlGrade_count grade)
            errorcount = L.pack (show $ kvtmlGrade_errorCount grade)
            date = L.pack $ formatTime defaultTimeLocale "%Y-%m-%d" (kvtmlGrade_date grade)
            fullTextStr = if kvtmlTrans_id translation == 0
                            then "        <full_text>" `L.append` fullText `L.append` "</full_text>\n"
                            else ""


getLessonsNode :: Kvtml -> L.Text
getLessonsNode kvtml =
        "  <lessons>\n" `L.append`
            L.concat lessonXmls `L.append`
        "  </lessons>\n"
    where
        lessons = kvtml_lessons kvtml
        lessonXmls = map (getLessonXml 4) lessons


getLessonXml :: Int -> KvtmlLesson -> L.Text
getLessonXml indent lesson =
        indentStr `L.append` "<container>\n" `L.append`
        indentStr `L.append` "  <name>" `L.append` name `L.append` "</name>\n" `L.append`
        childLessonXmls `L.append`
        entryIdXmls `L.append`
        indentStr `L.append` "</container>\n"
    where
        indentStr = L.pack (replicate indent ' ')
        name = L.fromStrict $ kvtmlLesson_name lesson
        childLessonXmls = L.concat $ map (getLessonXml (indent + 2)) (kvtmlLesson_lessons lesson)
        entryIdXmls = L.concat $ map getEntryIdXml (kvtmlLesson_entries lesson)
        getEntryIdXml entryId = indentStr `L.append` "  <entry id=\"" `L.append`
                              L.pack (show entryId) `L.append` "\"/>\n"
