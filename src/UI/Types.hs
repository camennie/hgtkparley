{-
hgtkparley -- A vocabulary learning system, tailored to Enligh/Japanese using KVTML
Written by Chris Mennie (chris at chrismennie.ca)
Copyright (C) 2015 Chris A. Mennie

License: Released under the GPL version 3 license. See the included LICENSE.
-}

module UI.Types (
    AlternatingRowLookup (..),
    MainWindowTreeListModelRow (..),
    GtkParleyWindow (..),
    QuizEntry (..),
    QuizEntryState (..),
    GtkPracticeWindow (..),
    QuizResults,
    QuizEntryAnsweredState (..),
    EditWindow (..),

    translationLookupIndex,
    isDarkTheme
)

where

import qualified Graphics.UI.Gtk as Gtk
import Graphics.Rendering.Pango.Font
import Data.Int
import Data.Bool
import Data.Text
import Kvtml.Kvtml
import qualified Data.HashMap.Strict as HM
import Prelude (IO, Show, Eq, Ord)
import Data.IORef
import System.Random.MWC

type QuizResults = HM.HashMap Int Bool


isDarkTheme :: Bool
isDarkTheme = True


data AlternatingRowLookup = AlternatingRowLookup
    {
        arl_lookup :: HM.HashMap Int Bool,
        arl_lastSet :: Bool
    }


data MainWindowTreeListModelRow = MainWindowTreeListModelRow
    {
        mwtlmr_name :: Text,
        mwtlmr_selected :: Bool,
        mwtlmr_enabled :: Bool,
        mwtlmr_id :: Int,
        mwtlmr_container :: KvtmlLesson
    }


data GtkParleyWindow = GtkParleyWindow
    {
        gpw_gtkwindow :: Gtk.Window,
        gpw_lessonTreeStore :: Gtk.TreeStore MainWindowTreeListModelRow,
        gpw_updateLessonTree :: Kvtml -> IO (),
        gpw_updateUIWithEnabledLessons :: HM.HashMap Text Bool -> IO (),
        gpw_saveEnabledStates :: Text -> IO (),
        gpw_getEnabledActiveLessons :: IO [KvtmlLesson],
        gpw_quizRef :: IORef Kvtml
    }


data EditWindow = EditWindow
    {
        ew_editWindow :: Gtk.Window,
        ew_lessonTreeStore :: Gtk.TreeStore MainWindowTreeListModelRow,
        ew_updateLessonTree :: Kvtml -> IO (),
        ew_quizRef :: IORef Kvtml
    }


data QuizEntryAnsweredState = AnsweredCorrectly | AnsweredCorrectlyAndResetErrors | AnsweredIncorrectly | Unanswered deriving (Eq, Ord, Show)


data QuizEntry = QuizEntry
    {
        qe_answeredState :: QuizEntryAnsweredState,
        qe_correctAnswers :: Int,
        qe_attempts :: Int,
        qe_entry :: KvtmlEntry,
        qe_fromText :: Text,
        qe_toText :: Text
    }


data QuizEntryState = UserEntry | EnterPressed | ShowingCorrectAnswer | ShowingWrongAnswer deriving (Show, Eq, Ord)


data GtkPracticeWindow = GtkPracticeWindow
    {
        gpw_mainwindow :: Gtk.Window,
        gpw_practicewindow :: Gtk.Window,
        gpw_wordLabel :: Gtk.Label,
        gpw_translationEntry :: Gtk.Entry,
        gpw_cycleLabel :: Gtk.Label,
        gpw_progressbar :: Gtk.ProgressBar,
        gpw_verifyButton :: Gtk.Widget,
        gpw_englishFont :: FontDescription,
        gpw_japaneseFont :: FontDescription,

        gpw_origQuestionCount :: IORef Int,

        gpw_currentQuiz :: IORef [QuizEntry],
        gpw_quizQueue :: IORef [QuizEntry],
        gpw_quizResults :: IORef QuizResults,
        gpw_entryState :: IORef QuizEntryState,
        gpw_isEJ :: IORef Bool,

        gpw_updateQuizWhenDoneFunc :: Bool -> QuizResults -> IO (),
        gpw_updateWindowPrefsFunc :: Int -> Int -> Int -> Int -> IO (),

        gpw_random :: GenIO
    }


--Note: This doesn't really belong here
translationLookupIndex :: Bool -> Int
translationLookupIndex True = 0
translationLookupIndex False = 1
