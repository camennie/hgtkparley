{-
hgtkparley -- A vocabulary learning system, tailored to Enligh/Japanese using KVTML
Written by Chris Mennie (chris at chrismennie.ca)
Copyright (C) 2015 Chris A. Mennie

License: Released under the GPL version 3 license. See the included LICENSE.
-}

module UI.GtkParleyWindow (
GtkParleyWindow (..),

initMainWidow

) where

import UI.Types
import qualified UI.LessonTree as LT
import UI.WordList
import UI.PracticeWindow
import Kvtml.Kvtml
import Quiz
import Preferences.Preferences

import qualified Graphics.UI.Gtk as Gtk
import qualified Data.HashMap.Strict as HM
import Data.Bool
import Data.Text hiding (length, concat)
import Data.Int
import Control.Monad hiding (forM , forM_ , mapM , mapM_ , msum , sequence , sequence_ )
import Control.Monad.Trans
import System.IO
import Prelude (($), (.), length, concat)
import Data.Time.LocalTime
import Data.IORef


updateUIWithEnabledLessons :: Gtk.TreeStore MainWindowTreeListModelRow -> HM.HashMap Text Bool -> IO ()
updateUIWithEnabledLessons treeStore enabledLessons =
    LT.updateUIWithEnabledLessons enabledLessons treeStore


initMainWidow :: Gtk.Builder -> IORef Kvtml -> GtkPracticeWindow -> EditWindow ->
                    (Int -> Int -> Int -> Int -> IO ()) -> Preferences -> IO GtkParleyWindow
initMainWidow builder kvtmlRef practicewindow editWindow updateWindowPrefs preferences = do
    wordListViewRet <- setUpWordListView builder "treeview_lesson" kvtmlRef (\_ _ _ _ -> return ())
    lessonTreeViewRet <- LT.setUpInfoTreeView builder "treeview" True
                            (updateWordListDueCounts kvtmlRef (wlvsr_updateWordListDueCounts wordListViewRet))
                            kvtmlRef

    LT.ltsr_setUpLessonTreeCallbacks lessonTreeViewRet (wlvsr_updateWordListViewHandler wordListViewRet)


    window <- Gtk.builderGetObject builder Gtk.castToWindow ("window_main" :: Text)
    _ <- window `Gtk.on` Gtk.deleteEvent $ handleWindowClose updateWindowPrefs window

    Gtk.windowMove window (pr_mainWindowX preferences) (pr_mainWindowY preferences)
    Gtk.windowSetDefaultSize window (pr_mainWindowWidth preferences) (pr_mainWindowHeight preferences)

    ejButton <- Gtk.builderGetObject builder Gtk.castToWidget ("button_ej" :: Text)
    jeButton <- Gtk.builderGetObject builder Gtk.castToWidget ("button_je" :: Text)

    let retConfig = GtkParleyWindow
                      {
                          gpw_gtkwindow = window,
                          gpw_lessonTreeStore = LT.ltsr_treeStore lessonTreeViewRet,
                          gpw_updateLessonTree = LT.updateLessonTree (LT.ltsr_treeStore lessonTreeViewRet),
                          gpw_updateUIWithEnabledLessons = updateUIWithEnabledLessons (LT.ltsr_treeStore lessonTreeViewRet),
                          gpw_saveEnabledStates = saveEnabledStates (LT.ltsr_treeStore lessonTreeViewRet),
                          gpw_getEnabledActiveLessons = LT.getEnabledActiveLessons (LT.ltsr_treeStore lessonTreeViewRet),
                          gpw_quizRef = kvtmlRef
                      }

    _ <- ejButton `Gtk.on` Gtk.buttonReleaseEvent $ handleEJButtonPress retConfig practicewindow preferences
    _ <- jeButton `Gtk.on` Gtk.buttonReleaseEvent $ handleJEButtonPress retConfig practicewindow preferences

    quizMenuItem <- Gtk.builderGetObject builder Gtk.castToMenuItem ("menuitem_quit" :: Text)
    _ <- quizMenuItem `Gtk.on` Gtk.menuItemActivated $ Gtk.mainQuit

    editMenuItem <- Gtk.builderGetObject builder Gtk.castToMenuItem ("menuitem_edit" :: Text)
    _ <- editMenuItem `Gtk.on` Gtk.menuItemActivated $ handleEditQuiz editWindow kvtmlRef

    _ <- ew_editWindow editWindow `Gtk.on` Gtk.deleteEvent $ handleEditWindowClose kvtmlRef retConfig

    return retConfig


handleEditWindowClose :: IORef Kvtml -> GtkParleyWindow -> Gtk.EventM Gtk.EAny Bool
handleEditWindowClose quizDataRef mainWindow = do
    -- FIXME: Disabling this for now, otherwise we need to fix the dropped selection states
    --liftIO $ do
    --    currentQuiz <- readIORef quizDataRef
    --    gpw_updateLessonTree mainWindow currentQuiz

    return False


handleEditQuiz :: EditWindow -> IORef Kvtml -> IO ()
handleEditQuiz editWindow quizDataRef = do
    currentQuiz <- readIORef quizDataRef
    ew_updateLessonTree editWindow currentQuiz
    Gtk.widgetShowAll $ ew_editWindow editWindow


handleWindowClose :: (Int -> Int -> Int -> Int -> IO ()) -> Gtk.Window -> Gtk.EventM Gtk.EAny Bool
handleWindowClose updateWindowPrefs window = do
    liftIO $ do
        (x, y) <- Gtk.windowGetPosition window
        (width, height) <- Gtk.windowGetSize window

        updateWindowPrefs width height x y
        Gtk.mainQuit

    return False


saveEnabledStates :: Gtk.TreeStore MainWindowTreeListModelRow -> Text -> IO ()
saveEnabledStates treeStore filename = do
    selectedFileData <- LT.getEnabledLessonStates treeStore
    saveSelectedFile selectedFileData filename


handleEJButtonPress :: GtkParleyWindow -> GtkPracticeWindow -> Preferences -> Gtk.EventM Gtk.EButton Bool
handleEJButtonPress windowConfig practicewindow preferences = do
    liftIO $ handleEJButtonPressCommon True windowConfig practicewindow preferences
    return True


handleJEButtonPress :: GtkParleyWindow -> GtkPracticeWindow -> Preferences -> Gtk.EventM Gtk.EButton Bool
handleJEButtonPress windowConfig practicewindow preferences = do
    liftIO $ handleEJButtonPressCommon False windowConfig practicewindow preferences
    return True


handleEJButtonPressCommon :: Bool -> GtkParleyWindow -> GtkPracticeWindow -> Preferences -> IO ()
handleEJButtonPressCommon isEJ windowConfig practicewindowConfig preferences = do
    kvtml <- readIORef (gpw_quizRef windowConfig)

    activeEnabledLessons <- LT.getEnabledActiveLessons (gpw_lessonTreeStore windowConfig)

    localTime <- getZonedTime

    let currentDate = (localDay . zonedTimeToLocalTime) localTime

    let dueEntries = getDueEntries kvtml isEJ activeEnabledLessons currentDate

    case dueEntries of
        [] -> return ()
        _ -> do
            Gtk.windowIconify $ gpw_gtkwindow windowConfig
            setupQuizWindow isEJ practicewindowConfig dueEntries preferences
            Gtk.windowPresent $ gpw_practicewindow practicewindowConfig

    return ()



updateWordListDueCounts :: IORef Kvtml -> (Int -> Int -> IO ()) -> Gtk.TreeStore MainWindowTreeListModelRow -> IO ()
updateWordListDueCounts kvtmlRef updateWordListDueCountLabels treeStore = do
    kvtml <- readIORef kvtmlRef
    activeEnabledLessons <- LT.getEnabledActiveLessons treeStore
    localTime <- getZonedTime

    let currentDate = (localDay . zonedTimeToLocalTime) localTime

    let dueEntriesEJ = length . concat $ getDueEntries kvtml True activeEnabledLessons currentDate
    let dueEntriesJE = length . concat $ getDueEntries kvtml False activeEnabledLessons currentDate

    updateWordListDueCountLabels dueEntriesEJ dueEntriesJE
