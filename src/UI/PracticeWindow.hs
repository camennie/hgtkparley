{-
hgtkparley -- A vocabulary learning system, tailored to Enligh/Japanese using KVTML
Written by Chris Mennie (chris at chrismennie.ca)
Copyright (C) 2015 Chris A. Mennie

License: Released under the GPL version 3 license. See the included LICENSE.
-}

module UI.PracticeWindow (
    GtkPracticeWindow (..),
    initPracticeWindow,
    setupQuizWindow
) where


import UI.Types
import Kvtml.Kvtml
import Preferences.Preferences

import qualified Graphics.UI.Gtk as Gtk
import Graphics.Rendering.Pango.Font
import qualified Data.HashMap.Strict as HM
import Data.Bool
import Data.Text hiding (map, splitAt, length, take, init, last, concat, filter)
import Data.Int
import Data.Maybe
import Data.Ord
import Data.Function
import Data.List (splitAt, (++), length, take, init, last, or, concat, filter)
import qualified Data.List as L
import Control.Monad hiding (forM , forM_ , mapM , mapM_ , msum , sequence , sequence_ )
import Control.Monad.Trans
import Data.Monoid
import System.IO
import Prelude ((-), (==), (/=), (+), (/), fromIntegral, Double, show)
import Data.IORef
import System.Random.MWC
import Data.Random
import Safe
import Control.Applicative
import Data.Traversable (sequence)
import Quiz (entryGrade)


green :: Gtk.Color
green = Gtk.Color 0 65535 0

red :: Gtk.Color
red = Gtk.Color 65535 0 0

normalTextColour :: Gtk.Color
normalTextColour = if isDarkTheme then Gtk.Color 58000 58000 58000 else Gtk.Color 0 0 0


getRandomFontHelper :: Text -> (Text, Double)
getRandomFontHelper randomFontItem =
        (fontName, fontSize)
    where
        fontItemWords = words randomFontItem
        fontNameHelper [] = "Arial"
        fontNameHelper fiws = unwords $ init fiws
        fontName = fontNameHelper fontItemWords

        fontSizeHelper [] = Just 56
        fontSizeHelper fiws = (readMay . unpack $ last fiws) :: Maybe Double
        fontSizeM = fontSizeHelper fontItemWords
        fontSize = fromMaybe 56 fontSizeM


getRandomFont :: [Text] -> GenIO -> IO (Text, Double)
getRandomFont [] _ = return ("Arial", 56)
getRandomFont fontList genIO = do
    randomFontItem <- runRVar (randomElement fontList) genIO
    return $ getRandomFontHelper randomFontItem


initPracticeWindow :: Gtk.Builder -> (Bool -> QuizResults -> IO ()) ->
                        (Int -> Int -> Int -> Int -> IO ()) -> Preferences -> IO GtkPracticeWindow
initPracticeWindow builder updateQuizWhenDoneFunc updateWindowPrefs preferences = do
    practicewindow <- Gtk.builderGetObject builder Gtk.castToWindow ("window_practice" :: Text)

    mainWindow <- Gtk.builderGetObject builder Gtk.castToWindow ("window_main" :: Text)

    cheatButton <- Gtk.builderGetObject builder Gtk.castToWidget ("button_skip_cheat" :: Text)
    skipButton <- Gtk.builderGetObject builder Gtk.castToWidget ("button_skip" :: Text)
    verifyButton <- Gtk.builderGetObject builder Gtk.castToWidget ("button_verify" :: Text)
    stopButton <- Gtk.builderGetObject builder Gtk.castToWidget ("button_stop" :: Text)

    progressbar <- Gtk.builderGetObject builder Gtk.castToProgressBar ("progressbar" :: Text)
    translationEntry <- Gtk.builderGetObject builder Gtk.castToEntry ("entry_translation" :: Text)

    cycleLabel <- Gtk.builderGetObject builder Gtk.castToLabel ("label_cycle" :: Text)
    wordLabel <- Gtk.builderGetObject builder Gtk.castToLabel ("label_word" :: Text)

    randomGenIO <- createSystemRandom

    (englishFontName, englishFontSize) <- getRandomFont (pr_englishFonts preferences) randomGenIO
    (japaneseFontName, japaneseFontSize) <- getRandomFont (pr_japaneseFonts preferences) randomGenIO

    englishFont <- fontDescriptionNew
    japaneseFont <- fontDescriptionNew

    fontDescriptionSetFamily englishFont englishFontName
    fontDescriptionSetFamily japaneseFont japaneseFontName
    fontDescriptionSetSize englishFont englishFontSize
    fontDescriptionSetSize japaneseFont japaneseFontSize

    quizIORef <- newIORef []
    quizQueueRef <- newIORef []
    quizResultsRef <- newIORef HM.empty
    entryStateRef <- newIORef UserEntry
    origQuestionCountRef <- newIORef 0
    isEJRef <- newIORef False

    Gtk.windowMove practicewindow (pr_practiceWindowX preferences) (pr_practiceWindowY preferences)
    Gtk.windowSetDefaultSize practicewindow (pr_practiceWindowWidth preferences) (pr_practiceWindowHeight preferences)

    let retConfig = GtkPracticeWindow
                    {
                        gpw_mainwindow = mainWindow,
                        gpw_practicewindow = practicewindow,
                        gpw_wordLabel = wordLabel,
                        gpw_translationEntry = translationEntry,
                        gpw_verifyButton = verifyButton,
                        gpw_cycleLabel = cycleLabel,
                        gpw_progressbar = progressbar,
                        gpw_englishFont = englishFont,
                        gpw_japaneseFont = japaneseFont,
                        gpw_currentQuiz = quizIORef,
                        gpw_quizQueue = quizQueueRef,
                        gpw_quizResults = quizResultsRef,
                        gpw_entryState = entryStateRef,
                        gpw_origQuestionCount = origQuestionCountRef,
                        gpw_isEJ = isEJRef,

                        gpw_updateQuizWhenDoneFunc = updateQuizWhenDoneFunc,
                        gpw_updateWindowPrefsFunc = updateWindowPrefs,

                        gpw_random = randomGenIO
                    }

    _ <- practicewindow `Gtk.on` Gtk.deleteEvent $ handlePracticeWindowClose retConfig

    _ <- cheatButton `Gtk.on` Gtk.buttonReleaseEvent $ handleCheatButtonPressEvent retConfig
    _ <- skipButton `Gtk.on` Gtk.buttonReleaseEvent $ handleSkipButtonPressEvent retConfig
    _ <- verifyButton `Gtk.on` Gtk.buttonReleaseEvent $ handleVerifyButtonPress retConfig
    _ <- stopButton `Gtk.on` Gtk.buttonReleaseEvent $ handleStopButtonPress retConfig

    _ <- cheatButton `Gtk.on` Gtk.keyReleaseEvent $ handleCheatButtonPressEventK retConfig
    _ <- skipButton `Gtk.on` Gtk.keyReleaseEvent $ handleSkipButtonPressEventK retConfig
    _ <- verifyButton `Gtk.on` Gtk.keyReleaseEvent $ handleVerifyButtonPressK retConfig
    _ <- stopButton `Gtk.on` Gtk.keyReleaseEvent $ handleStopButtonPressK retConfig

    _ <- translationEntry `Gtk.on` Gtk.keyReleaseEvent $ handleQuizKeypressEvent retConfig

    return retConfig


wrapEntry :: Bool -> KvtmlEntry -> QuizEntry
wrapEntry isEJ entry =
    QuizEntry
        {
            qe_answeredState = Unanswered,
            qe_correctAnswers = startingAnswers,
            qe_attempts = 0,
            qe_entry = entry,
            qe_fromText = fromText,
            qe_toText = toText
        }

    where
        curGrade = entryGrade isEJ entry
        startingAnswers = if curGrade > 5 then 1 else 0

        translationM = HM.lookup (1 - translationLookupIndex isEJ) (kvtmlEntry_translations entry)
        translation2M = HM.lookup (translationLookupIndex isEJ) (kvtmlEntry_translations entry)

        fromText = maybe "" kvtmlTrans_text translationM
        toText = maybe "" kvtmlTrans_text translation2M


setupQuizWindow :: Bool -> GtkPracticeWindow -> [[KvtmlEntry]] -> Preferences -> IO ()
setupQuizWindow isEJ windowConfig dueEntriesGrouped preferences = do
        dueEntriesShuffledGrouped <- sequence $ fmap (\e -> runRVar (shuffle e) (gpw_random windowConfig)) dueEntriesGrouped
        let dueEntriesPartitionShuffled = concat dueEntriesShuffledGrouped
        let quizEntries = take 50 $ fmap (wrapEntry isEJ) dueEntriesPartitionShuffled

        shuffledQuizEntries <- shuffleQuizAllEntries quizEntries (gpw_random windowConfig)

        writeIORef quizQueueRef []
        writeIORef isEJRef isEJ
        writeIORef currentQuizRef shuffledQuizEntries
        writeIORef (gpw_origQuestionCount windowConfig) (length shuffledQuizEntries)
        writeIORef (gpw_quizResults windowConfig) HM.empty
        writeIORef (gpw_entryState windowConfig) UserEntry

        Gtk.windowMove practiceWindow (pr_practiceWindowX preferences) (pr_practiceWindowY preferences)
        Gtk.windowSetDefaultSize practiceWindow (pr_practiceWindowWidth preferences) (pr_practiceWindowHeight preferences)

        if isEJ
            then do
                Gtk.widgetModifyFont (gpw_translationEntry windowConfig) (Just $ gpw_japaneseFont windowConfig)
                Gtk.widgetModifyFont (gpw_wordLabel windowConfig) (Just $ gpw_englishFont windowConfig)
            else do
                Gtk.widgetModifyFont (gpw_translationEntry windowConfig) (Just $ gpw_englishFont windowConfig)
                Gtk.widgetModifyFont (gpw_wordLabel windowConfig) (Just $ gpw_japaneseFont windowConfig)

        resetEntryBox windowConfig
        showNextEntry windowConfig shuffledQuizEntries
    where
        practiceWindow = gpw_practicewindow windowConfig
        currentQuizRef = gpw_currentQuiz windowConfig
        quizQueueRef = gpw_quizQueue windowConfig
        isEJRef = gpw_isEJ windowConfig


ensureLastEntryNotShuffledToFirstEntry :: Int -> [QuizEntry] -> IO [QuizEntry]
ensureLastEntryNotShuffledToFirstEntry _ [] = return []
ensureLastEntryNotShuffledToFirstEntry lastEntryId entries@(fe:es) =
        if lastEntryId /= firstEntryId
            then return entries
            else return $ es ++ [fe]
    where
        firstEntryId = kvtmlEntry_id $ qe_entry fe


shuffleQuizAllEntries :: [QuizEntry] -> GenIO -> IO [QuizEntry]
shuffleQuizAllEntries [] _ = return []
shuffleQuizAllEntries entries genIO =
        shuffleQuizAllEntriesHelper entries genIO >>=
            ensureLastEntryNotShuffledToFirstEntry lastEntryId
    where
        lastEntryId = kvtmlEntry_id $ qe_entry (last entries)


shuffleQuizAllEntriesHelper :: [QuizEntry] -> GenIO -> IO [QuizEntry]
shuffleQuizAllEntriesHelper entries = runRVar (shuffle entries)


-- Shuffle with locality
shuffleQuizEntries :: [QuizEntry] -> GenIO -> IO [QuizEntry]
shuffleQuizEntries [] _ = return []
shuffleQuizEntries entries genIO =
        shuffleQuizEntriesHelper entries genIO >>=
            ensureLastEntryNotShuffledToFirstEntry lastEntryId
    where
        lastEntryId = kvtmlEntry_id $ qe_entry (last entries)


shuffleQuizEntriesHelper :: [QuizEntry] -> GenIO -> IO [QuizEntry]
shuffleQuizEntriesHelper entries genIO
    | length entries < 11 = shuffleQuizAllEntries entries genIO
    | otherwise = do
        let (firstPart, lastTen) = splitAt (length entries - 10) entries
        shuffledFirstPart <- shuffleQuizAllEntries firstPart genIO
        let (shuffledTen, remainingEntries) = splitAt 10 (shuffledFirstPart ++ lastTen)
        shuffledRemaining <- shuffleQuizAllEntries remainingEntries genIO

        return $ shuffledTen ++ shuffledRemaining


resetEntryBox :: GtkPracticeWindow -> IO ()
resetEntryBox windowConfig = do
        writeIORef entryStateRef UserEntry

        Gtk.widgetModifyFg translationEntry Gtk.StateNormal normalTextColour
        Gtk.widgetModifyText translationEntry Gtk.StateNormal normalTextColour
        Gtk.entrySetText translationEntry ("" :: Text)
        Gtk.widgetGrabFocus translationEntry
    where
        entryStateRef = gpw_entryState windowConfig
        translationEntry = gpw_translationEntry windowConfig


showNextEntry :: GtkPracticeWindow -> [QuizEntry] -> IO ()
showNextEntry _ [] = return ()
showNextEntry windowConfig (firstEntry:es) = do
        quizQueue <- readIORef quizQueueRef
        writeIORef (gpw_currentQuiz windowConfig) (updatedEntry:es)

        origQuizCount <- readIORef origQuizCountRef

        Gtk.labelSetText (gpw_cycleLabel windowConfig) (show $ qe_correctAnswers updatedEntry)

        let answered = origQuizCount - ((length es + 1) + length quizQueue)
        let answeredDbl :: Double = fromIntegral answered
        let progress = answeredDbl / fromIntegral origQuizCount

        Gtk.progressBarSetFraction progressBar progress
        Gtk.progressBarSetText progressBar $ pack (show answered) `append` " / " `append` pack (show origQuizCount)

        Gtk.labelSetText (gpw_wordLabel windowConfig) (qe_fromText updatedEntry)
    where
        quizQueueRef = gpw_quizQueue windowConfig
        origQuizCountRef = gpw_origQuestionCount windowConfig

        updatedEntry = firstEntry { qe_attempts = qe_attempts firstEntry + 1 }

        progressBar = gpw_progressbar windowConfig


setNextQuestion :: GtkPracticeWindow -> IO ()
setNextQuestion windowConfig = do
        resetEntryBox windowConfig

        currentQuiz <- readIORef currentQuizRef
        quizQueue <- readIORef quizQueueRef

        updateQuizQueues windowConfig currentQuiz quizQueue (gpw_random windowConfig)

        updatedQuiz <- readIORef currentQuizRef

        showNextEntry windowConfig updatedQuiz
    where
        currentQuizRef = gpw_currentQuiz windowConfig
        quizQueueRef = gpw_quizQueue windowConfig


updateQuizQueues :: GtkPracticeWindow -> [QuizEntry] -> [QuizEntry] -> GenIO -> IO ()
updateQuizQueues windowConfig [] [] _ = handleStop windowConfig
updateQuizQueues windowConfig [] quizQueue genIO = do
        shuffledQuiz <- shuffleQuizEntries quizQueue genIO

        writeIORef currentQuizRef shuffledQuiz
        writeIORef quizQueueRef []
    where
        currentQuizRef = gpw_currentQuiz windowConfig
        quizQueueRef = gpw_quizQueue windowConfig


updateQuizQueues windowConfig [firstEntry] quizQueue genIO =
        if qe_correctAnswers firstEntry > 2
            then do
                updateQuizResults windowConfig firstEntry
                updateQuizQueues windowConfig [] quizQueue (gpw_random windowConfig)
            else do
                shuffledQuiz <- shuffleQuizEntries (quizQueue ++ [firstEntry]) genIO
                writeIORef currentQuizRef shuffledQuiz
                writeIORef quizQueueRef []
    where
        currentQuizRef = gpw_currentQuiz windowConfig
        quizQueueRef = gpw_quizQueue windowConfig

updateQuizQueues windowConfig (firstEntry:es) quizQueue _ = do
        writeIORef currentQuizRef es

        -- If correctAnswers > 2 then the entry is dropped from the lists
        if qe_correctAnswers firstEntry < 3
            then writeIORef quizQueueRef (quizQueue ++ [firstEntry])
            else updateQuizResults windowConfig firstEntry
    where
        currentQuizRef = gpw_currentQuiz windowConfig
        quizQueueRef = gpw_quizQueue windowConfig


updateQuizResults :: GtkPracticeWindow -> QuizEntry -> IO ()
updateQuizResults windowConfig firstEntry = do
        quizResults <- readIORef quizResultsRef

        let updatedResultMap = HM.insert entryId (qe_answeredState firstEntry == AnsweredCorrectly) quizResults
        writeIORef quizResultsRef updatedResultMap
    where
        quizResultsRef = gpw_quizResults windowConfig
        entryId = kvtmlEntry_id $ qe_entry firstEntry


handlePracticeWindowClose :: GtkPracticeWindow -> Gtk.EventM Gtk.EAny Bool
handlePracticeWindowClose windowConfig = liftIO $ handleStop windowConfig >> return True


handleCheatButtonPressEvent :: GtkPracticeWindow -> Gtk.EventM Gtk.EButton Bool
handleCheatButtonPressEvent windowConfig = liftIO $ handleCheatButtonPress windowConfig False >> return True

handleCheatButtonPressEventK :: GtkPracticeWindow -> Gtk.EventM Gtk.EKey Bool
handleCheatButtonPressEventK windowConfig = liftIO $ handleCheatButtonPress windowConfig False >> return True


handleSkipButtonPressEvent :: GtkPracticeWindow -> Gtk.EventM Gtk.EButton Bool
handleSkipButtonPressEvent windowConfig = liftIO $ handleSkipButtonPress windowConfig >> return True

handleSkipButtonPressEventK :: GtkPracticeWindow -> Gtk.EventM Gtk.EKey Bool
handleSkipButtonPressEventK windowConfig = liftIO $ handleSkipButtonPress windowConfig >> return True


handleCheatButtonPress :: GtkPracticeWindow -> Bool -> IO ()
handleCheatButtonPress windowConfig resetErrors = do
        quizEntries <- readIORef currentQuizRef
        handleCheatSkipCommon quizEntries (+1) ShowingCorrectAnswer green windowConfig resetErrors
    where
        currentQuizRef = gpw_currentQuiz windowConfig


handleSkipButtonPress :: GtkPracticeWindow -> IO ()
handleSkipButtonPress windowConfig = do
        quizEntries <- readIORef currentQuizRef
        handleCheatSkipCommon quizEntries (const 0) ShowingWrongAnswer red windowConfig False
    where
        currentQuizRef = gpw_currentQuiz windowConfig


handleCheatSkipCommon :: [QuizEntry] -> (Int -> Int) -> QuizEntryState -> Gtk.Color -> GtkPracticeWindow -> Bool -> IO ()
handleCheatSkipCommon [] _ _ _ windowConfig _ = resetEntryBox windowConfig --Note: I don't think this should be possible
handleCheatSkipCommon (fe:es) caFunc entryState textColour windowConfig resetErrors = do
        currentEntryState <- readIORef currentEntryStateRef   -- FIXME: ... update currentEntryStateRef with reset state

        if currentEntryState == entryState
            then do
                writeIORef currentQuizRef newQuizEntries
                setNextQuestion windowConfig
            else do
                writeIORef currentEntryStateRef entryState

                Gtk.widgetGrabFocus (gpw_verifyButton windowConfig)
                Gtk.widgetModifyFg translationEntry Gtk.StateNormal textColour
                Gtk.widgetModifyText translationEntry Gtk.StateNormal textColour
                Gtk.entrySetText translationEntry (qe_toText fe)
    where
        currentEntryStateRef = gpw_entryState windowConfig

        answeredState AnsweredIncorrectly False _ = AnsweredIncorrectly
        answeredState AnsweredIncorrectly True _ = AnsweredCorrectly
        answeredState AnsweredCorrectly _ ShowingWrongAnswer = AnsweredIncorrectly
        answeredState AnsweredCorrectly _ _ = AnsweredCorrectly
        answeredState Unanswered True _ = AnsweredCorrectly
        answeredState Unanswered False ShowingWrongAnswer = AnsweredIncorrectly
        answeredState Unanswered False _ = AnsweredCorrectly
        answeredState AnsweredCorrectlyAndResetErrors _ ShowingWrongAnswer = AnsweredIncorrectly
        answeredState AnsweredCorrectlyAndResetErrors _ _ = AnsweredCorrectly


        newEntry = fe {
                            qe_correctAnswers = caFunc $ qe_correctAnswers fe,
                            qe_answeredState = answeredState (qe_answeredState fe) resetErrors entryState
                          }
        newQuizEntries = newEntry : es

        currentQuizRef = gpw_currentQuiz windowConfig

        translationEntry = gpw_translationEntry windowConfig


handleVerifyButtonPress :: GtkPracticeWindow -> Gtk.EventM Gtk.EButton Bool
handleVerifyButtonPress windowConfig = liftIO $ do
    entryState <- readIORef (gpw_entryState windowConfig)
    handleVerifyButtonPressCommon entryState windowConfig
    return True


handleVerifyButtonPressK :: GtkPracticeWindow -> Gtk.EventM Gtk.EKey Bool
handleVerifyButtonPressK windowConfig = liftIO $ do
        entryState <- readIORef entryStateRef
        handleVerifyButtonPressCommon entryState windowConfig
        return True
    where
        entryStateRef = gpw_entryState windowConfig


handleVerifyButtonPressCommon :: QuizEntryState -> GtkPracticeWindow -> IO ()
handleVerifyButtonPressCommon ShowingCorrectAnswer windowConfig = handleCheatButtonPress windowConfig False
handleVerifyButtonPressCommon ShowingWrongAnswer windowConfig = handleSkipButtonPress windowConfig


handleVerifyButtonPressCommon _ windowConfig = do
        baseEntryText <- strip <$> Gtk.entryGetText translationEntry

        let resetErrors = or (fmap (`isPrefixOf` baseEntryText) ["!!!", "！！！"])
        let entryText = if resetErrors then drop 3 baseEntryText else baseEntryText

        quizEntries <- readIORef currentQuizRef

        let toText = maybe "" qe_toText (headMay quizEntries)

        if compareEntryValues toText entryText
            then handleCheatButtonPress windowConfig resetErrors
            else handleSkipButtonPress windowConfig
    where
        translationEntry = gpw_translationEntry windowConfig
        currentQuizRef = gpw_currentQuiz windowConfig


removeComments :: Text -> Text
removeComments str =
    let (beforeCommentR, afterR) = span (/= '{') str in
    let beforeComment = strip beforeCommentR in
    let after = strip afterR in

    if null after
        then beforeComment
        else let (_, postCommentR) = strip <$> span (/= '}') after in
                let postComment = strip (dropWhile (== '}') postCommentR) in

                if null postComment
                    then beforeComment
                    else beforeComment <> " " <> removeComments postComment


groupWords :: [Text] -> [Text]
groupWords strs =
    let (before, after) = L.span (\s -> s /= "and" && s /= "or") strs in

    if L.null after
        then [unwords before]
        else unwords before : groupWords (L.drop 1 after)


compareEntryValues :: Text -> Text -> Bool
compareEntryValues toText entryText =
        let toTextNoComments = removeComments $ toLower toText in
        let entryTextNoComments = removeComments $ toLower entryText in

        let toWords = groupWords $ words toTextNoComments in
        let entryWords = groupWords $ words entryTextNoComments in

        let sortedTo = L.sort toWords in
        let sortedEntry = L.sort entryWords in

        sortedTo == sortedEntry


handleStopButtonPress :: GtkPracticeWindow -> Gtk.EventM Gtk.EButton Bool
handleStopButtonPress windowConfig = liftIO $ handleStop windowConfig >> return True

handleStopButtonPressK :: GtkPracticeWindow -> Gtk.EventM Gtk.EKey Bool
handleStopButtonPressK windowConfig = liftIO $ handleStop windowConfig >> return True


handleStop :: GtkPracticeWindow -> IO ()
handleStop windowConfig = do
    isEJ <- readIORef $ gpw_isEJ windowConfig
    quizResults <- readIORef $ gpw_quizResults windowConfig

    gpw_updateQuizWhenDoneFunc windowConfig isEJ quizResults

    (x, y) <- Gtk.windowGetPosition (gpw_practicewindow windowConfig)
    (width, height) <- Gtk.windowGetSize (gpw_practicewindow windowConfig)

    gpw_updateWindowPrefsFunc windowConfig width height x y

    Gtk.widgetHide (gpw_practicewindow windowConfig)
    Gtk.windowPresent (gpw_mainwindow windowConfig)


handleQuizKeypressEvent :: GtkPracticeWindow -> Gtk.EventM Gtk.EKey Bool
handleQuizKeypressEvent windowConfig = do
        keyVal <- Gtk.eventKeyVal

        let isReturn = keyVal == 0xff0d {-gdk_return-} ||
                       keyVal == 0xff8d {-gdk_kp_enter-}
        let isSpace = keyVal `L.elem` spaces

        entryText <- liftIO $ Gtk.entryGetText translationEntry
        isEJ <- liftIO $ readIORef isEJRef

        liftIO $ unless (null entryText) $ do
                let entryStateRef = gpw_entryState windowConfig
                entryState <- readIORef entryStateRef

                -- This is a little hacky
                if entryState /= ShowingWrongAnswer
                    then do
                        when (isReturn && entryState /= UserEntry) (handleVerifyButtonPressCommon entryState windowConfig)
                        when (isReturn && entryState == UserEntry && isEJ) (writeIORef entryStateRef EnterPressed)
                        when (isReturn && entryState == UserEntry && not isEJ) (handleVerifyButtonPressCommon entryState windowConfig)
                        when (not isReturn && entryState /= UserEntry) (writeIORef entryStateRef UserEntry)
                    else
                        when isSpace (handleVerifyButtonPressCommon entryState windowConfig)

        return True
    where
        translationEntry = gpw_translationEntry windowConfig
        isEJRef = gpw_isEJ windowConfig

        spaces = [{-GDK_KEY_KP_Space-} 0xff80,
                  {-GDK_KEY_ISO_Partial_Space_Left-} 0xfe25,
                  {-GDK_KEY_ISO_Partial_Space_Right-} 0xfe26,
                  {-GDK_KEY_space-} 0x020,
                  {-GDK_KEY_nobreakspace-} 0x0a0,
                  {-GDK_KEY_emspace-} 0xaa1,
                  {-GDK_KEY_enspace-} 0xaa2,
                  {-GDK_KEY_em3space-} 0xaa3,
                  {-GDK_KEY_em4space-} 0xaa4,
                  {-GDK_KEY_digitspace-} 0xaa5,
                  {-GDK_KEY_punctspace-} 0xaa6,
                  {-GDK_KEY_thinspace-} 0xaa7,
                  {-GDK_KEY_hairspace-} 0xaa8]
