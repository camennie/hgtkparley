{-
hgtkparley -- A vocabulary learning system, tailored to Enligh/Japanese using KVTML
Written by Chris Mennie (chris at chrismennie.ca)
Copyright (C) 2015 Chris A. Mennie

License: Released under the GPL version 3 license. See the included LICENSE.
-}

module UI.EditWindow (

initEditWindow

) where

import UI.Types
import qualified UI.LessonTree as LT
import UI.WordList
import Kvtml.Kvtml
import Preferences.Preferences
import Prelude (($), (+), show, (==), (*))
import qualified Graphics.UI.Gtk as Gtk
import Graphics.Rendering.Pango.Font
import Data.Bool
import Data.Text hiding (length)
import Control.Monad hiding (forM , forM_ , mapM , mapM_ , msum , sequence , sequence_ )
import Control.Monad.Trans
import System.IO
import Data.IORef
import Data.Maybe
import Data.Int
import Safe


-- add word list selection -> edit box filling






initEditWindow :: Gtk.Builder -> IORef Kvtml -> IO () -> Preferences -> IO EditWindow
initEditWindow builder kvtmlRef saveQuizFunc preferences = do
    japaneseEntry <- Gtk.builderGetObject builder Gtk.castToEntry ("new_item_japanese_entry" :: Text)
    fullTextEntry <- Gtk.builderGetObject builder Gtk.castToEntry ("new_item_fulltext_entry" :: Text)
    englishEntry <- Gtk.builderGetObject builder Gtk.castToEntry ("new_item_english_entry" :: Text)
    itemIdEntry <- Gtk.builderGetObject builder Gtk.castToEntry ("new_item_id_entry" :: Text)

    japaneseFont <- fontDescriptionNew
    fontDescriptionSetFamily japaneseFont ("MS PMincho" :: Text)
    fontDescriptionSetSize japaneseFont 56
    Gtk.widgetModifyFont (Gtk.castToWidget japaneseEntry) (Just japaneseFont)
    Gtk.widgetModifyFont (Gtk.castToWidget fullTextEntry) (Just japaneseFont)

    wordListViewRet <- setUpWordListView builder "edit_treeview_lesson" kvtmlRef
                            (updateFieldsOnRowSelection japaneseEntry fullTextEntry englishEntry itemIdEntry)
    lessonTreeViewRet <- LT.setUpInfoTreeView builder "edit_treeview" False (\_ -> return ()) kvtmlRef

    LT.ltsr_setUpLessonTreeCallbacks lessonTreeViewRet (wlvsr_updateWordListViewHandler wordListViewRet)

    _ <- itemIdEntry `Gtk.on` Gtk.focus $ handleItemIdEntryFocus itemIdEntry kvtmlRef

    window <- Gtk.builderGetObject builder Gtk.castToWindow ("edit_window" :: Text)

    Gtk.windowMove window (pr_mainWindowX preferences) (pr_mainWindowY preferences)
    Gtk.windowSetDefaultSize window (pr_mainWindowWidth preferences) (pr_mainWindowHeight preferences)

    _ <- window `Gtk.on` Gtk.deleteEvent $ handleWindowClose saveQuizFunc

    let retConfig = EditWindow
                      {
                          ew_editWindow = window,
                          ew_lessonTreeStore = LT.ltsr_treeStore lessonTreeViewRet,
                          ew_updateLessonTree = LT.updateLessonTree (LT.ltsr_treeStore lessonTreeViewRet),
                          ew_quizRef = kvtmlRef
                      }

    -- FIXME: I'd really like it to scroll correctly after the new row is added
    wordListScrolledWindow <- Gtk.builderGetObject builder Gtk.castToScrolledWindow ("edit_treeview_scrolledwindow" :: Text)

    _ <- itemIdEntry `Gtk.on` Gtk.keyReleaseEvent $
                handleItemIdKeypressEvent kvtmlRef (LT.ltsr_treeView lessonTreeViewRet) retConfig
                                          wordListViewRet japaneseEntry fullTextEntry englishEntry itemIdEntry
                                          wordListScrolledWindow

    _ <- japaneseEntry `Gtk.on` Gtk.keyReleaseEvent $ scrollOnKeypressEvent wordListScrolledWindow
    _ <- fullTextEntry `Gtk.on` Gtk.keyReleaseEvent $ scrollOnKeypressEvent wordListScrolledWindow
    _ <- englishEntry `Gtk.on` Gtk.keyReleaseEvent $ scrollOnKeypressEvent wordListScrolledWindow

    return retConfig


updateFieldsOnRowSelection :: Gtk.Entry -> Gtk.Entry -> Gtk.Entry -> Gtk.Entry -> Text -> Text -> Text -> Int -> IO ()
updateFieldsOnRowSelection japaneseEntry fullTextEntry englishEntry itemIdEntry japaneseText fullText englishText itemId = do
    Gtk.entrySetText japaneseEntry japaneseText
    Gtk.entrySetText fullTextEntry fullText
    Gtk.entrySetText englishEntry englishText
    Gtk.entrySetText itemIdEntry (show itemId)
    return ()


scrollOnKeypressEvent :: Gtk.ScrolledWindow -> Gtk.EventM Gtk.EKey Bool
scrollOnKeypressEvent wordListScrolledWindow = do
    liftIO $ do
                vadjust <- Gtk.scrolledWindowGetVAdjustment wordListScrolledWindow
                vadjustUpper <- Gtk.adjustmentGetUpper vadjust
                Gtk.adjustmentSetValue vadjust (2.0 * vadjustUpper)
                Gtk.widgetQueueDraw (Gtk.castToWidget wordListScrolledWindow)

    return True


handleItemIdKeypressEvent :: IORef Kvtml -> Gtk.TreeView -> EditWindow -> WordListViewSetupRet ->
                                Gtk.Entry -> Gtk.Entry -> Gtk.Entry -> Gtk.Entry ->　Gtk.ScrolledWindow ->
                                Gtk.EventM Gtk.EKey Bool
handleItemIdKeypressEvent quizDataRef treeView windowConfig wordListViewRet
                          japaneseEntry fullTextEntry englishEntry itemIdEntry　wordListScrolledWindow = do
    keyVal <- Gtk.eventKeyVal

    let isReturn = keyVal == 0xff0d {-gdk_return-} ||
                   keyVal == 0xff8d {-gdk_kp_enter-}

    liftIO $ when isReturn $ do
            japaneseText <- liftIO $ Gtk.entryGetText japaneseEntry
            fullText <- liftIO $ Gtk.entryGetText fullTextEntry
            englishText <- liftIO $ Gtk.entryGetText englishEntry
            itemIdText <- liftIO $ Gtk.entryGetText itemIdEntry
            let itemIdM :: Maybe Int = readMay itemIdText

            when (isJust itemIdM && not (null japaneseText) && not (null englishText)) $ do
                let itemId = fromJust itemIdM

                wlvsr_forceAddEntry wordListViewRet japaneseText fullText englishText itemId

                parentLessonPath <- LT.getCurrentlySelectedLessonPath treeView (ew_lessonTreeStore windowConfig)

                currentQuiz <- readIORef quizDataRef

                let updatedQuiz = addNewQuizEntry currentQuiz japaneseText fullText englishText itemId parentLessonPath

                writeIORef quizDataRef updatedQuiz

                Gtk.entrySetText japaneseEntry ("" :: Text)
                Gtk.entrySetText fullTextEntry ("" :: Text)
                Gtk.entrySetText englishEntry ("" :: Text)
                Gtk.entrySetText itemIdEntry ("" :: Text)

                Gtk.widgetGrabFocus japaneseEntry

            return ()

    liftIO $ do
                vadjust <- Gtk.scrolledWindowGetVAdjustment wordListScrolledWindow
                vadjustUpper <- Gtk.adjustmentGetUpper vadjust
                Gtk.adjustmentSetValue vadjust (2.0 * vadjustUpper)
                Gtk.widgetQueueDraw (Gtk.castToWidget wordListScrolledWindow)

    return True


handleItemIdEntryFocus :: Gtk.Entry -> IORef Kvtml -> Gtk.DirectionType -> IO Bool
handleItemIdEntryFocus itemIdEntry quizDataRef _ = do
    newItemIdText <- Gtk.entryGetText itemIdEntry

    when (null newItemIdText) $ do
        currentQuiz <- readIORef quizDataRef
        let newEntryId = 1 + kvtmlGetMaxEntryId currentQuiz
        Gtk.entrySetText itemIdEntry (show newEntryId)

    return False


handleWindowClose :: IO () -> Gtk.EventM Gtk.EAny Bool
handleWindowClose saveQuizFunc = do
    liftIO saveQuizFunc
    return False
