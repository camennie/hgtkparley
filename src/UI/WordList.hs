{-
hgtkparley -- A vocabulary learning system, tailored to Enligh/Japanese using KVTML
Written by Chris Mennie (chris at chrismennie.ca)
Copyright (C) 2015 Chris A. Mennie

License: Released under the GPL version 3 license. See the included LICENSE.
-}

module UI.WordList (
    WordListViewSetupRet (..),
    setUpWordListView
) where

import UI.Common
import UI.Types
import Kvtml.Kvtml

import qualified Graphics.UI.Gtk as Gtk
import Graphics.UI.Gtk (AttrOp((:=)))
import Graphics.Rendering.Pango.Font
import Data.Text (Text)
import Prelude (($), (.), Show, fst, snd, show)
import Data.Int
import Data.Maybe
import Data.Bool
import Data.IORef
import Control.Monad hiding (forM , forM_ , mapM , mapM_ , msum , sequence , sequence_ )
import Control.Monad.Trans
import Data.Ord
import System.IO
import qualified Data.HashMap.Strict as HM
import Data.Conduit
import Data.List hiding (
    all, and, any, concat, concatMap, find, foldl,
    foldl', foldl1, foldr, foldr1, mapAccumL,
    mapAccumR, maximum, maximumBy, minimum,
    minimumBy ,notElem ,or ,product ,sum )
import Data.Traversable
import Data.Foldable
import Data.Monoid
import Control.Applicative


data WordListModelRow = WordListModelRow
    {
        wlmr_japanese :: Text,
        wlmr_fullText :: Text,
        wlmr_english :: Text,
        wlmr_entityId :: Int,
        wlmr_id :: Int
    } deriving (Show)


data WordListViewSetupRet = WordListViewSetupRet
    {
        wlvsr_updateWordListViewHandler :: KvtmlLesson -> Sink Int IO (),
        wlvsr_updateWordListDueCounts :: Int -> Int -> IO (),
        wlvsr_forceAddEntry :: Text -> Text -> Text -> Int -> IO ()
    }


setUpWordListView :: Gtk.Builder -> Text -> IORef Kvtml ->
                     (Text -> Text -> Text -> Int -> IO ()) -> IO WordListViewSetupRet
setUpWordListView builder wordTreeViewName kvtmlRef rowSelectionCallback = do
    treeview <- Gtk.builderGetObject builder Gtk.castToTreeView wordTreeViewName
    Gtk.treeViewSetGridLines treeview Gtk.TreeViewGridLinesVertical

    japaneseColumn <- Gtk.treeViewColumnNew
    Gtk.treeViewColumnSetTitle japaneseColumn ("Japanese" :: Text)
    Gtk.treeViewColumnSetResizable japaneseColumn True
    Gtk.treeViewColumnSetAlignment japaneseColumn 0.5

    --fullTextColumn <- Gtk.treeViewColumnNew
    --Gtk.treeViewColumnSetTitle fullTextColumn ("Full Text" :: Text)
    --Gtk.treeViewColumnSetResizable fullTextColumn True
    --Gtk.treeViewColumnSetAlignment fullTextColumn 0.5

    englishColumn <- Gtk.treeViewColumnNew
    Gtk.treeViewColumnSetTitle englishColumn ("English" :: Text)
    Gtk.treeViewColumnSetResizable englishColumn True
    Gtk.treeViewColumnSetAlignment englishColumn 0.5

    --_ <- Gtk.treeViewAppendColumn treeview fullTextColumn
    _ <- Gtk.treeViewAppendColumn treeview japaneseColumn
    _ <- Gtk.treeViewAppendColumn treeview englishColumn

    Gtk.treeViewColumnSetSizing japaneseColumn Gtk.TreeViewColumnFixed
    --Gtk.treeViewColumnSetSizing fullTextColumn Gtk.TreeViewColumnFixed
    Gtk.treeViewColumnSetSizing englishColumn Gtk.TreeViewColumnFixed

    Gtk.treeViewColumnSetFixedWidth japaneseColumn 500 --Note: Hack because otherwise column widths aren't correctly set
    Gtk.treeViewColumnSetMinWidth japaneseColumn 300

    listStore <- Gtk.listStoreNew ([] :: [WordListModelRow])
    Gtk.treeViewSetModel treeview listStore

    englishFont <- fontDescriptionNew
    japaneseFont <- fontDescriptionNew

    fontDescriptionSetFamily englishFont ("Arial" :: Text)
    fontDescriptionSetFamily japaneseFont ("MS PMincho" :: Text)
    fontDescriptionSetSize englishFont 56
    fontDescriptionSetSize japaneseFont 56

    englishTextCellRenderer <- Gtk.cellRendererTextNew
    japaneseTextCellRenderer <- Gtk.cellRendererTextNew
    --fullTextCellRenderer <- Gtk.cellRendererTextNew

    altColorRef <- newIORef $ AlternatingRowLookup HM.empty False

    Gtk.cellLayoutPackStart englishColumn englishTextCellRenderer True
    Gtk.cellLayoutSetAttributes englishColumn englishTextCellRenderer listStore
            (\row ->
                    [Gtk.cellText := wlmr_english row, Gtk.cellTextFontDesc := englishFont,
                      Gtk.cellBackground := getCellBackgroundColor (wlmr_id row) altColorRef])

    Gtk.cellLayoutPackStart japaneseColumn japaneseTextCellRenderer True
    Gtk.cellLayoutSetAttributes japaneseColumn japaneseTextCellRenderer listStore
            (\row ->
                    [Gtk.cellText := wlmr_japanese row, Gtk.cellTextFontDesc := japaneseFont,
                      Gtk.cellBackground := getCellBackgroundColor (wlmr_id row) altColorRef])

    --Gtk.cellLayoutPackStart fullTextColumn fullTextCellRenderer True
    --Gtk.cellLayoutSetAttributes fullTextColumn fullTextCellRenderer listStore
    --        (\row ->
    --                [Gtk.cellText := wlmr_fullText row, Gtk.cellTextFontDesc := japaneseFont,
    --                  Gtk.cellBackground := getCellBackgroundColor (wlmr_id row) altColorRef])

    treeSelection <- Gtk.treeViewGetSelection treeview
    _ <- treeSelection `Gtk.on` Gtk.treeSelectionSelectionChanged $
                handleWordListRowSelected listStore rowSelectionCallback treeSelection

    let updateWordListViewHandler = updateWordListView kvtmlRef listStore

    return WordListViewSetupRet
                   {
                       wlvsr_updateWordListViewHandler = updateWordListViewHandler,
                       wlvsr_updateWordListDueCounts = updateWordListDueCounts englishColumn japaneseColumn,
                       wlvsr_forceAddEntry = forceAddEntry listStore
                   }


handleWordListRowSelected :: Gtk.ListStore WordListModelRow -> (Text -> Text -> Text -> Int -> IO ()) ->
                             Gtk.TreeSelection -> IO ()
handleWordListRowSelected wordTreeModel rowSelectionCallback treeSelection = do
    selectedRowM <- Gtk.treeSelectionGetSelected treeSelection

    maybe
        (return ())
        (\selectedTreeIter -> do
            treePath <- Gtk.treeModelGetPath wordTreeModel selectedTreeIter
            wlmr <- Gtk.listStoreGetValue wordTreeModel (head treePath)
            rowSelectionCallback (wlmr_japanese wlmr) (wlmr_fullText wlmr) (wlmr_english wlmr) (wlmr_entityId wlmr)
            return ()
        )
        selectedRowM


forceAddEntry :: Gtk.ListStore WordListModelRow -> Text -> Text -> Text -> Int -> IO ()
forceAddEntry listStore japaneseText fullText englishText entityId = do
        _ <- Gtk.listStoreAppend listStore newRow

        return ()
    where
        newRow = WordListModelRow
                    {
                        wlmr_japanese = japaneseText,
                        wlmr_fullText = fullText,
                        wlmr_english = englishText,
                        wlmr_entityId = entityId,
                        wlmr_id = 0
                    }


updateWordListDueCounts :: Gtk.TreeViewColumn -> Gtk.TreeViewColumn -> Int -> Int -> IO ()
updateWordListDueCounts englishColumn japaneseColumn e2jCount j2eCount = do
    Gtk.treeViewColumnSetTitle englishColumn ("English (" ++ show e2jCount ++ ")")
    Gtk.treeViewColumnSetTitle japaneseColumn ("Japanese (" ++ show j2eCount ++ ")")


createNewWordListModelRow :: (Text, Text, Text, Int) -> Sink Int IO WordListModelRow
createNewWordListModelRow (japanese, fullText, english, entityId) = do
    nextId <- await
    return WordListModelRow
        {
            wlmr_japanese = japanese,
            wlmr_fullText = fullText,
            wlmr_english = english,
            wlmr_entityId = entityId,
            wlmr_id = fromMaybe 0 nextId
        }


updateWordListView :: IORef Kvtml -> Gtk.ListStore WordListModelRow ->
                        KvtmlLesson -> Sink Int IO ()
updateWordListView kvtmlRef listStore lesson = do
        kvtml <- liftIO $ readIORef kvtmlRef
        liftIO $ Gtk.listStoreClear listStore

        let entries = kvtmlEntryIdsToEntry kvtml entryIds

        let entryTranslations :: [(Text, Text, Text, Int)] = collectTranslations entries
        rows <- mapM createNewWordListModelRow entryTranslations

        liftIO $ mapM_ (Gtk.listStoreAppend listStore) rows

        return ()

    where
        entryIds = collectEntries lesson

        collectEntries curLesson =
            kvtmlLesson_entries curLesson ++ concatMap collectEntries (kvtmlLesson_lessons curLesson)

        collectTranslations =
            map (\entry ->
                    let translationsKV :: [(Int, KvtmlTranslation)] = HM.toList (kvtmlEntry_translations entry) in
                    let sortedTranslationsKV = sortBy (comparing fst) translationsKV in
                    let fullText = mconcat $ (kvtmlTrans_fullText . snd) <$> sortedTranslationsKV in
                    let (japanese, english) = getEnglishJapanese $ map (kvtmlTrans_text . snd) sortedTranslationsKV in

                    (japanese, fullText, english, kvtmlEntry_id entry)
                )

        getEnglishJapanese (japanese:english:_) = (japanese, english)
        getEnglishJapanese _ = ("invalid" :: Text, "invalid" :: Text)
