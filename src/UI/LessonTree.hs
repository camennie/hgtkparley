{-
hgtkparley -- A vocabulary learning system, tailored to Enligh/Japanese using KVTML
Written by Chris Mennie (chris at chrismennie.ca)
Copyright (C) 2015 Chris A. Mennie

License: Released under the GPL version 3 license. See the included LICENSE.
-}

module UI.LessonTree (
    LessonTreeSetupRet (..),

    setUpInfoTreeView,
    updateUIWithEnabledLessons,
    updateLessonTree,
    getEnabledLessonStates,
    getEnabledActiveLessons,
    getCurrentlySelectedLessonPath
) where

import UI.Common
import UI.Types
import Kvtml.Kvtml


import qualified Graphics.UI.Gtk as Gtk
import Graphics.UI.Gtk (AttrOp((:=)))
import Data.Text hiding (zip)
import Prelude (($), (==))
import Data.Int
import Data.Maybe
import Data.Bool
import Data.IORef
import Control.Monad hiding (forM , forM_ , mapM , mapM_ , msum , sequence , sequence_ )
import Control.Monad.Trans
import Data.String (String)
import Data.Tree
import System.IO
import qualified Data.HashMap.Strict as HM
import Data.Conduit
import qualified Data.Conduit.List as CL
import Data.List hiding (
    all, and, any, concat, concatMap, find, foldl,
    foldl', foldl1, foldr, foldr1, mapAccumL,
    mapAccumR, maximum, maximumBy, minimum,
    minimumBy ,notElem ,or ,product ,sum, null )
import Data.Traversable
import Data.Foldable hiding (null)
import Safe


idSource :: Source IO Int -- produces a stream of Ints
idSource = CL.sourceList [1..]


data LessonTreeSetupRet = LessonTreeSetupRet
    {
        ltsr_treeStore :: Gtk.TreeStore MainWindowTreeListModelRow,
        ltsr_treeView :: Gtk.TreeView,
        ltsr_updateLessonTree :: Kvtml -> IO (),
        ltsr_setUpLessonTreeCallbacks :: (KvtmlLesson -> Sink Int IO ()) -> IO ()
    }


setUpInfoTreeView :: Gtk.Builder -> Text -> Bool ->
                        (Gtk.TreeStore MainWindowTreeListModelRow -> IO ()) -> IORef Kvtml ->
                        IO LessonTreeSetupRet
setUpInfoTreeView builder treeViewName showSelectedEnabledColumns triggerUpdateColumnCounts quizDataRef = do
    treeview <- Gtk.builderGetObject builder Gtk.castToTreeView treeViewName
    Gtk.treeViewSetGridLines treeview Gtk.TreeViewGridLinesVertical

    nameColumn <- Gtk.treeViewColumnNew
    Gtk.treeViewColumnSetTitle nameColumn ("Name" :: Text)
    Gtk.treeViewColumnSetResizable nameColumn True

    selectedColumn <- Gtk.treeViewColumnNew
    Gtk.treeViewColumnSetTitle selectedColumn ("Selected" :: Text)
    Gtk.treeViewColumnSetResizable selectedColumn True
    Gtk.treeViewColumnSetAlignment nameColumn 0.0

    enabledColumn <- Gtk.treeViewColumnNew
    Gtk.treeViewColumnSetTitle enabledColumn ("Enabled" :: Text)
    Gtk.treeViewColumnSetResizable enabledColumn True
    Gtk.treeViewColumnSetExpand enabledColumn True

    blankColumn <- Gtk.treeViewColumnNew
    Gtk.treeViewColumnSetTitle blankColumn ("" :: Text)

    textCellRenderer <- Gtk.cellRendererTextNew
    selectedToggleCellRenderer <- Gtk.cellRendererToggleNew
    enabledToggleCellRenderer <- Gtk.cellRendererToggleNew

    treeStore <- Gtk.treeStoreNew ([] :: [Tree MainWindowTreeListModelRow])

    altColorRef <- newIORef $ AlternatingRowLookup HM.empty False

    Gtk.cellLayoutPackStart blankColumn textCellRenderer True
    Gtk.cellLayoutSetAttributes blankColumn textCellRenderer treeStore
            (\row ->
                    [Gtk.cellText := ("" :: Text),
                      Gtk.cellBackground := getCellBackgroundColor (mwtlmr_id row) altColorRef])

    Gtk.cellLayoutPackStart nameColumn textCellRenderer True
    Gtk.cellLayoutSetAttributes nameColumn textCellRenderer treeStore
            (\row ->
                    [Gtk.cellText := mwtlmr_name row,
                      Gtk.cellBackground := getCellBackgroundColor (mwtlmr_id row) altColorRef])

    Gtk.cellLayoutPackStart selectedColumn selectedToggleCellRenderer True
    Gtk.cellLayoutSetAttributes selectedColumn selectedToggleCellRenderer treeStore
            (\row ->
                    [Gtk.cellToggleActive := mwtlmr_selected row,
                      Gtk.cellBackground := getCellBackgroundColor (mwtlmr_id row) altColorRef])

    Gtk.cellLayoutPackStart enabledColumn enabledToggleCellRenderer True
    Gtk.cellLayoutSetAttributes enabledColumn enabledToggleCellRenderer treeStore
            (\row ->
                    [Gtk.cellToggleActive := mwtlmr_enabled row,
                      Gtk.cellBackground := getCellBackgroundColor (mwtlmr_id row) altColorRef])

    _ <- Gtk.treeViewAppendColumn treeview nameColumn

    when showSelectedEnabledColumns $ do
        _ <- Gtk.treeViewAppendColumn treeview selectedColumn
        _ <- Gtk.treeViewAppendColumn treeview enabledColumn
        return ()

    _ <- Gtk.treeViewAppendColumn treeview blankColumn

    Gtk.treeViewSetModel treeview treeStore

    _ <- treeview `Gtk.on` Gtk.rowExpanded $ clearAltColourMap altColorRef
    _ <- treeview `Gtk.on` Gtk.rowCollapsed $ clearAltColourMap altColorRef

    _ <- enabledToggleCellRenderer `Gtk.on` Gtk.cellToggled $ handleEnabledToggle treeStore
    _ <- selectedToggleCellRenderer `Gtk.on` Gtk.cellToggled $ handleSelectedToggle treeStore

    return LessonTreeSetupRet
            {
                ltsr_treeStore = treeStore,
                ltsr_treeView = treeview,
                ltsr_updateLessonTree = updateLessonTree treeStore,
                ltsr_setUpLessonTreeCallbacks = setUpLessonTreeCallbacks builder
                                                showSelectedEnabledColumns treeStore treeview
                                                (triggerUpdateColumnCounts treeStore) quizDataRef
            }


updateUIWithEnabledLessons :: HM.HashMap Text Bool -> Gtk.TreeStore MainWindowTreeListModelRow -> IO ()
updateUIWithEnabledLessons enabledLessons treeStore =
    Gtk.treeModelForeach treeStore
        (\treeiter -> do
            treePath <- Gtk.treeModelGetPath treeStore treeiter
            mwtlmr <- Gtk.treeStoreGetValue treeStore treePath

            let foundLesson = HM.lookup (kvtmlLesson_fullName $ mwtlmr_container mwtlmr) enabledLessons
            maybe
                (return False)
                (\enabled -> do
                    let updatedRow = mwtlmr { mwtlmr_enabled = enabled }
                    Gtk.treeStoreSetValue treeStore treePath updatedRow

                    return False
                )
                foundLesson
        )


{- Update lesson tree stuff -}

addLessonToTree :: Gtk.TreeStore MainWindowTreeListModelRow -> [Int] -> (Int, KvtmlLesson) -> Sink Int IO ()
addLessonToTree treeStore path (pathid, lesson) = do
    nextId <- await

    let row = MainWindowTreeListModelRow
                  {
                      mwtlmr_name = kvtmlLesson_name lesson,
                      mwtlmr_selected = False,
                      mwtlmr_enabled = False,
                      mwtlmr_id = fromMaybe 0 nextId,
                      mwtlmr_container = lesson
                  }

    liftIO $ Gtk.treeStoreInsert treeStore path (-1) row

    addLessonsToTree treeStore (path ++ [pathid]) (kvtmlLesson_lessons lesson)


addLessonsToTree :: Gtk.TreeStore MainWindowTreeListModelRow -> [Int] -> [KvtmlLesson] -> Sink Int IO ()
addLessonsToTree treeStore path lessons = do
        _ <- mapM (addLessonToTree treeStore path) pathIdLessonPairs
        return ()
    where
        pathIdLessonPairs :: [(Int, KvtmlLesson)] = zip [0..] lessons :: [(Int, KvtmlLesson)]


updateLessonTree :: Gtk.TreeStore MainWindowTreeListModelRow -> Kvtml -> IO ()
updateLessonTree treeStore kvtml = do
        liftIO $ Gtk.treeStoreClear treeStore
        idSource $$ addLessonsToTree treeStore [] lessons
    where
        lessons = kvtml_lessons kvtml


{- row selection handler -}

handleLessonRowSelected :: Gtk.TreeStore MainWindowTreeListModelRow -> (KvtmlLesson -> Sink Int IO ()) ->
                            Gtk.TreeSelection -> IO ()
handleLessonRowSelected lessonTreeModel updateWordListViewHandler treeSelection = do
    selectedRowM <- Gtk.treeSelectionGetSelected treeSelection

    maybe
        (return ())
        (\selectedTreeIter -> do
            treePath <- Gtk.treeModelGetPath lessonTreeModel selectedTreeIter
            mwtlmr <- Gtk.treeStoreGetValue lessonTreeModel treePath
            idSource $$ updateWordListViewHandler (mwtlmr_container mwtlmr)
            return ()
        )
        selectedRowM


{- updating model when selection/enabled checkboxes are toggled -}

handleEnabledToggle :: Gtk.TreeStore MainWindowTreeListModelRow -> String -> IO ()
handleEnabledToggle treeStore pathStr = do
        mwtlmr <- Gtk.treeStoreGetValue treeStore treePath
        let curEnabled = mwtlmr_enabled mwtlmr
        let updatedRow = mwtlmr { mwtlmr_enabled = not curEnabled }

        Gtk.treeStoreSetValue treeStore treePath updatedRow
    where
        treePath = Gtk.stringToTreePath (pack pathStr)


handleSelectedToggle :: Gtk.TreeStore MainWindowTreeListModelRow -> String -> IO ()
handleSelectedToggle treeStore pathStr = do
        mwtlmr <- Gtk.treeStoreGetValue treeStore treePath
        let curSelected = mwtlmr_selected mwtlmr
        let updatedRow = mwtlmr { mwtlmr_selected = not curSelected }

        Gtk.treeStoreSetValue treeStore treePath updatedRow
    where
        treePath = Gtk.stringToTreePath (pack pathStr)


{- Lesson selection button and row click callbacks -}

setUpLessonTreeCallbacks :: Gtk.Builder -> Bool -> Gtk.TreeStore MainWindowTreeListModelRow ->
                            Gtk.TreeView -> IO () -> IORef Kvtml ->
                            (KvtmlLesson -> Sink Int IO ()) -> IO ()
setUpLessonTreeCallbacks builder bindLessonSelectionButtons treeStore treeView
                            triggerUpdateColumnCounts quizDataRef updateWordListViewHandler = do
    when bindLessonSelectionButtons $ do
        bothButton <- Gtk.builderGetObject builder Gtk.castToWidget ("button_km_both" :: Text)
        kanjiButton <- Gtk.builderGetObject builder Gtk.castToWidget ("button_km_kanji" :: Text)
        kanaButton <- Gtk.builderGetObject builder Gtk.castToWidget ("button_km_kana" :: Text)
        jmButton <- Gtk.builderGetObject builder Gtk.castToWidget ("button_jm" :: Text)
        katakanaButton <- Gtk.builderGetObject builder Gtk.castToWidget ("button_katakana" :: Text)
        clearButton <- Gtk.builderGetObject builder Gtk.castToWidget ("button_clear" :: Text)

        _ <- kanjiButton `Gtk.on` Gtk.buttonReleaseEvent $ handleKanjiButtonPress treeStore triggerUpdateColumnCounts
        _ <- kanaButton `Gtk.on` Gtk.buttonReleaseEvent $ handleKanaButtonPress treeStore triggerUpdateColumnCounts
        _ <- jmButton `Gtk.on` Gtk.buttonReleaseEvent $ handleJMButtonPress treeStore triggerUpdateColumnCounts
        _ <- katakanaButton `Gtk.on` Gtk.buttonReleaseEvent $ handleKatakanaButtonPress treeStore triggerUpdateColumnCounts
        _ <- bothButton `Gtk.on` Gtk.buttonReleaseEvent $ handleBothButtonPress treeStore triggerUpdateColumnCounts
        _ <- clearButton `Gtk.on` Gtk.buttonReleaseEvent $ handleClearButtonPress treeStore triggerUpdateColumnCounts
        return ()

    treeSelection <- Gtk.treeViewGetSelection treeView
    _ <- treeSelection `Gtk.on` Gtk.treeSelectionSelectionChanged $
             handleLessonRowSelected treeStore updateWordListViewHandler treeSelection

    unless bindLessonSelectionButtons $ do
        addLessonButton <- Gtk.builderGetObject builder Gtk.castToWidget ("add_lesson_button" :: Text)
        newLessonNameEntry <- Gtk.builderGetObject builder Gtk.castToEntry ("new_lesson_entry" :: Text)

        _ <- addLessonButton `Gtk.on` Gtk.buttonReleaseEvent $
                                handleAddNewLesson newLessonNameEntry treeView treeStore quizDataRef
        return ()
    return ()


handleAddNewLesson :: Gtk.Entry -> Gtk.TreeView -> Gtk.TreeStore MainWindowTreeListModelRow ->
                        IORef Kvtml -> Gtk.EventM Gtk.EButton Bool
handleAddNewLesson newLessonNameEntry treeView treeStore quizDataRef = do
    liftIO $ do
        entryText <- Gtk.entryGetText newLessonNameEntry
        Gtk.entrySetText newLessonNameEntry empty

        selectedRows <- Gtk.treeViewGetSelection treeView
        selectedPaths <- Gtk.treeSelectionGetSelectedRows selectedRows

        case headMay selectedPaths of
            Nothing -> return ()

            Just firstPath ->
                unless (null entryText) $ do
                    -- Add new lesson to tree view
                    selectedLessonModelRow <- Gtk.treeStoreGetValue treeStore firstPath

                    let row = MainWindowTreeListModelRow
                                  {
                                      mwtlmr_name = entryText,
                                      mwtlmr_selected = False,
                                      mwtlmr_enabled = False,
                                      mwtlmr_id = 0,
                                      mwtlmr_container = mwtlmr_container selectedLessonModelRow
                                  }

                    liftIO $ Gtk.treeStoreInsert treeStore firstPath (-1) row

                    -- Add new lesson to quiz data
                    let newLesson = KvtmlLesson
                                    {
                                        kvtmlLesson_parent = kvtmlLesson_name $ mwtlmr_container selectedLessonModelRow,
                                        kvtmlLesson_name = entryText,
                                        kvtmlLesson_lessons = [],
                                        kvtmlLesson_entries = []
                                    }

                    parentLessonPath <- getLessonPath treeStore firstPath
                    currentQuiz <- readIORef quizDataRef
                    let updatedQuiz = addNewChildLesson currentQuiz parentLessonPath newLesson

                    writeIORef quizDataRef updatedQuiz

    return True


getCurrentlySelectedLessonPath :: Gtk.TreeView -> Gtk.TreeStore MainWindowTreeListModelRow -> IO [Text]
getCurrentlySelectedLessonPath treeView treeStore = do
    selectedRows <- Gtk.treeViewGetSelection treeView
    selectedPaths <- Gtk.treeSelectionGetSelectedRows selectedRows

    maybe (return []) (getLessonPath treeStore) (headMay selectedPaths)


getLessonPath :: Gtk.TreeStore MainWindowTreeListModelRow -> Gtk.TreePath -> IO [Text]
getLessonPath treeStore treePath = do
        let initAcc :: (Gtk.TreePath, [Text]) = ([], [])
        (_, pathNameAcc) <- foldrM buildLessonPathHelper initAcc (Data.List.reverse treePath)
        return pathNameAcc
    where
        buildLessonPathHelper :: Int -> (Gtk.TreePath, [Text]) -> IO (Gtk.TreePath, [Text])
        buildLessonPathHelper pathPart (pathAcc :: Gtk.TreePath, pathNameAcc :: [Text]) = do
            let newPathAcc = pathAcc ++ [pathPart]
            treeLesson <- Gtk.treeStoreGetValue treeStore newPathAcc
            let lessonName = mwtlmr_name treeLesson

            return (newPathAcc, pathNameAcc ++ [lessonName])



getRowFromTreeIter :: Gtk.TreeIter -> Gtk.TreeStore MainWindowTreeListModelRow -> IO MainWindowTreeListModelRow
getRowFromTreeIter treeiter treeStore =
    Gtk.treeModelGetPath treeStore treeiter >>= Gtk.treeStoreGetValue treeStore


getNamedTopLevelIter :: Text -> Gtk.TreeStore MainWindowTreeListModelRow -> Maybe Gtk.TreeIter -> IO (Maybe Gtk.TreeIter)
getNamedTopLevelIter _ _ Nothing = return Nothing
getNamedTopLevelIter name treeStore (Just curIter) = do
    row <- getRowFromTreeIter curIter treeStore
    if mwtlmr_name row == name
        then return (Just curIter)
        else Gtk.treeModelIterNext treeStore curIter >>= getNamedTopLevelIter name treeStore


handleClearButtonPress :: Gtk.TreeStore MainWindowTreeListModelRow -> IO () -> Gtk.EventM Gtk.EButton Bool
handleClearButtonPress treeStore triggerUpdateColumnCounts = do
        liftIO $ Gtk.treeModelForeach treeStore setSelectedStateB
        liftIO triggerUpdateColumnCounts
        return True
    where
        setSelectedStateB treeIter = setSelectedState treeStore False treeIter >> return False


handleBothButtonPress :: Gtk.TreeStore MainWindowTreeListModelRow -> IO () -> Gtk.EventM Gtk.EButton Bool
handleBothButtonPress treeStore triggerUpdateColumnCounts = do
    _ <- handleClearButtonPress treeStore triggerUpdateColumnCounts
    liftIO $ handleButtonPressCommon "漢字" treeStore triggerUpdateColumnCounts
    liftIO $ handleButtonPressCommon "かな" treeStore triggerUpdateColumnCounts
    return True


handleKanjiButtonPress :: Gtk.TreeStore MainWindowTreeListModelRow -> IO () -> Gtk.EventM Gtk.EButton Bool
handleKanjiButtonPress treeStore triggerUpdateColumnCounts = do
    _ <- handleClearButtonPress treeStore triggerUpdateColumnCounts
    liftIO $ handleButtonPressCommon "漢字" treeStore triggerUpdateColumnCounts
    return True


handleKanaButtonPress :: Gtk.TreeStore MainWindowTreeListModelRow -> IO () -> Gtk.EventM Gtk.EButton Bool
handleKanaButtonPress treeStore triggerUpdateColumnCounts = do
    _ <- handleClearButtonPress treeStore triggerUpdateColumnCounts
    liftIO $ handleButtonPressCommon "かな" treeStore triggerUpdateColumnCounts
    return True


handleJMButtonPress :: Gtk.TreeStore MainWindowTreeListModelRow -> IO () -> Gtk.EventM Gtk.EButton Bool
handleJMButtonPress treeStore triggerUpdateColumnCounts = do
    _ <- handleClearButtonPress treeStore triggerUpdateColumnCounts
    liftIO $ handleButtonPressCommon "Japanese in Mangaland" treeStore triggerUpdateColumnCounts
    return True


handleKatakanaButtonPress :: Gtk.TreeStore MainWindowTreeListModelRow -> IO () -> Gtk.EventM Gtk.EButton Bool
handleKatakanaButtonPress treeStore triggerUpdateColumnCounts = do
    _ <- handleClearButtonPress treeStore triggerUpdateColumnCounts
    liftIO $ handleButtonPressCommon "カタカナ" treeStore triggerUpdateColumnCounts
    return True


handleButtonPressCommon :: Text -> Gtk.TreeStore MainWindowTreeListModelRow -> IO () -> IO ()
handleButtonPressCommon topLevelName treeStore triggerUpdateColumnCounts = do
    firstIterM <- Gtk.treeModelGetIterFirst treeStore
    topLevelIterM <- getNamedTopLevelIter topLevelName treeStore firstIterM

    case topLevelIterM of
        Nothing -> return ()
        Just topLevelIter -> do
            let newState = True

            setSelectedState treeStore newState topLevelIter

            topLevelChildrenM <- Gtk.treeModelIterChildren treeStore topLevelIter
            maybe (return ()) (setRecursiveSelectedStates treeStore newState) topLevelChildrenM

    triggerUpdateColumnCounts


setSelectedState :: Gtk.TreeStore MainWindowTreeListModelRow -> Bool -> Gtk.TreeIter -> IO ()
setSelectedState treeStore newState treeIter = do
    row <- getRowFromTreeIter treeIter treeStore
    treePath <- Gtk.treeModelGetPath treeStore treeIter

    let updatedRow = row { mwtlmr_selected = newState }
    Gtk.treeStoreSetValue treeStore treePath updatedRow


setRecursiveSelectedStates :: Gtk.TreeStore MainWindowTreeListModelRow -> Bool -> Gtk.TreeIter -> IO ()
setRecursiveSelectedStates treeStore newState treeIter = do
    setSelectedState treeStore newState treeIter

    maybeChildrenIter <- Gtk.treeModelIterChildren treeStore treeIter
    maybe (return ()) (setRecursiveSelectedStates treeStore newState) maybeChildrenIter

    maybeSiblingIter <- Gtk.treeModelIterNext treeStore treeIter
    maybe (return ()) (setRecursiveSelectedStates treeStore newState) maybeSiblingIter


getEnabledLessonStates :: Gtk.TreeStore MainWindowTreeListModelRow -> IO SelectedFileData
getEnabledLessonStates treeStore = do
    firstIterM <- Gtk.treeModelGetIterFirst treeStore

    maybe
        (return HM.empty)
        (getEnabledLessonStatesRec treeStore HM.empty)
        firstIterM


getEnabledLessonStatesRec :: Gtk.TreeStore MainWindowTreeListModelRow -> SelectedFileData -> Gtk.TreeIter -> IO SelectedFileData
getEnabledLessonStatesRec treeStore selectedFileData treeIter = do
    row <- getRowFromTreeIter treeIter treeStore
    let name = kvtmlLesson_fullName $ mwtlmr_container row
    let enabled = mwtlmr_enabled row

    let updatedMap = HM.insert name enabled selectedFileData

    maybeChildrenIter <- Gtk.treeModelIterChildren treeStore treeIter
    updatedChildrenMap <- maybe (return updatedMap) (getEnabledLessonStatesRec treeStore updatedMap) maybeChildrenIter

    maybeSiblingIter <- Gtk.treeModelIterNext treeStore treeIter
    maybe (return updatedChildrenMap) (getEnabledLessonStatesRec treeStore updatedChildrenMap) maybeSiblingIter


getEnabledActiveLessons :: Gtk.TreeStore MainWindowTreeListModelRow -> IO [KvtmlLesson]
getEnabledActiveLessons treeStore = do
    firstIterM <- Gtk.treeModelGetIterFirst treeStore

    maybe
        (return [])
        (getEnabledActiveLessonsRec treeStore [])
        firstIterM


getEnabledActiveLessonsRec :: Gtk.TreeStore MainWindowTreeListModelRow -> [KvtmlLesson] -> Gtk.TreeIter -> IO [KvtmlLesson]
getEnabledActiveLessonsRec treeStore acc treeIter = do
    row <- getRowFromTreeIter treeIter treeStore

    let enabled = mwtlmr_enabled row
    let selected = mwtlmr_selected row

    let updatedList = if enabled && selected
                        then acc ++ [mwtlmr_container row]
                        else acc

    maybeChildrenIter <- Gtk.treeModelIterChildren treeStore treeIter
    updatedChildrenList <- maybe (return updatedList) (getEnabledActiveLessonsRec treeStore updatedList) maybeChildrenIter

    maybeSiblingIter <- Gtk.treeModelIterNext treeStore treeIter
    maybe (return updatedChildrenList) (getEnabledActiveLessonsRec treeStore updatedChildrenList) maybeSiblingIter
