{-
hgtkparley -- A vocabulary learning system, tailored to Enligh/Japanese using KVTML
Written by Chris Mennie (chris at chrismennie.ca)
Copyright (C) 2015 Chris A. Mennie

License: Released under the GPL version 3 license. See the included LICENSE.
-}

module UI.Common (
    AlternatingRowLookup,
    getCellBackgroundColor,
    clearAltColourMap
)

where

import UI.Types

import qualified Graphics.UI.Gtk as Gtk
import Data.Text
import Prelude (($))
import Data.Int
import Data.Maybe
import Data.Bool
import Data.IORef
import Control.Monad hiding (forM , forM_ , mapM , mapM_ , msum , sequence , sequence_ )
import System.IO
import qualified Data.HashMap.Strict as HM
import System.IO.Unsafe


cellBackgroundColour :: Text
cellBackgroundColour = if isDarkTheme then "#3d3d65" else "#aaaacc"

cellBackgroundColour2 :: Text
cellBackgroundColour2= if isDarkTheme then "#6e6e99" else "#ddddff"


getCellBackgroundColor :: Int -> IORef AlternatingRowLookup -> Text
getCellBackgroundColor id altColorRef = unsafePerformIO $ do
    altColorMap <- readIORef altColorRef
    let lookupmap = arl_lookup altColorMap
    let lastBoolVal = arl_lastSet altColorMap

    colorBool <- case HM.lookup id lookupmap of
                    Just altBool -> return altBool

                    Nothing -> do
                        let updatedMap = HM.insert id (not lastBoolVal) lookupmap
                        writeIORef altColorRef $ AlternatingRowLookup updatedMap (not lastBoolVal)
                        return (not lastBoolVal)

    if colorBool
        then return cellBackgroundColour
        else return cellBackgroundColour2


clearAltColourMap :: IORef AlternatingRowLookup -> Gtk.TreeIter -> Gtk.TreePath -> IO ()
clearAltColourMap altColorRef _ _ =
        writeIORef altColorRef emptyRowLookup
    where
        emptyRowLookup = AlternatingRowLookup HM.empty False
