{-
hgtkparley -- A vocabulary learning system, tailored to Enligh/Japanese using KVTML
Written by Chris Mennie (chris at chrismennie.ca)
Copyright (C) 2015 Chris A. Mennie

License: Released under the GPL version 3 license. See the included LICENSE.
-}

module XMLHelpers

where

import  Data.List hiding (
    all, and, any, concat, concatMap, find, foldl,
    foldl', foldl1, foldr, foldr1, mapAccumL,
    mapAccumR, maximum, maximumBy, minimum,
    minimumBy ,notElem ,or ,product ,sum )
import Control.Monad hiding (forM , forM_ , mapM , mapM_ , msum , sequence , sequence_ )
import Data.Traversable
import Data.Foldable
import Kvtml.Types
import Text.XML
import Data.Maybe
import Data.Time
import Data.Bool
import Data.Text hiding (zip, map, filter, find, head)
import Prelude ((==), ($), (.))


parseDayM :: Text -> Maybe Day
parseDayM dateStr =
        liftM utctDay parsedDateM
    where
        baseDateStr = head $ split (=='T') dateStr
        parsedDateM = parseTimeM True defaultTimeLocale "%Y-%m-%d" (unpack baseDateStr) :: Maybe UTCTime


parseDay :: Text -> Day
parseDay dateStr = fromMaybe defaultKvtmlDate $ parseDayM dateStr


getElement :: Node -> Maybe Element
getElement (NodeElement element) = Just element
getElement _ = Nothing


getNamedChildNodesM :: Text -> Element -> Maybe [Element]
getNamedChildNodesM childName parentNode =
        foundElements
    where
        foundNodes = filter (isNamedElement . getElement) $ elementNodes parentNode
        foundElements = mapM getElement foundNodes

        isNamedElement childElement =
            isJust childElement && nameLocalName (elementName (fromJust childElement)) ==  childName


getNamedChildNodeM :: Text -> Element -> Maybe Element
getNamedChildNodeM childName parentNode =
        maybe Nothing getElement foundNodeM
    where
        foundNodeM = find (isNamedElement . getElement) $ elementNodes parentNode

        isNamedElement childElement =
            isJust childElement && nameLocalName (elementName (fromJust childElement)) ==  childName


getOptionalElementTextNodeM :: Element -> Text
getOptionalElementTextNodeM node =
        maybe "" getTextElement foundNodeM
    where
        foundNodeM = find (isJust . getTextElementM) $ elementNodes node

        getTextElementM (NodeContent text) = Just text
        getTextElementM _ = Nothing

        getTextElement (NodeContent text) = text
        getTextElement _ = ""


getElementTextNodeM :: Element -> Maybe Text
getElementTextNodeM node =
        maybe Nothing getTextElement foundNodeM
    where
        foundNodeM = find (isJust . getTextElement) $ elementNodes node

        getTextElement (NodeContent text) = Just text
        getTextElement _ = Nothing


getNodeTextNodeM :: Node -> Maybe Text
getNodeTextNodeM node = getElement node >>= getElementTextNodeM


getNamedChildNodeTextM :: Text -> Element -> Maybe Text
getNamedChildNodeTextM name node =
        maybe Nothing getElementTextNodeM childNodeElementM
    where
        childNodeElementM = getNamedChildNodeM name node
