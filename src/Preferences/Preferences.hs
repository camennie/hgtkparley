{-
hgtkparley -- A vocabulary learning system, tailored to Enligh/Japanese using KVTML
Written by Chris Mennie (chris at chrismennie.ca)
Copyright (C) 2015 Chris A. Mennie

License: Released under the GPL version 3 license. See the included LICENSE.
-}

module Preferences.Preferences (
    Preferences (..),

    loadPreferencesFile,
    savePreferencesFile,

    defaultPreferences
)
where

import XMLHelpers

import Control.Monad hiding (forM , forM_ , mapM , mapM_ , msum , sequence , sequence_ )
import Text.XML
import Data.Maybe
import qualified Data.Text as T
import Data.Int
import Prelude (IO, ($), (.), show)
import Safe
import qualified Data.Text.Lazy.IO as DTIO
import qualified Data.Text.Lazy as L


data  Preferences = Preferences
    {
        pr_englishFonts :: [T.Text],
        pr_japaneseFonts :: [T.Text],
        pr_mainWindowWidth :: Int,
        pr_mainWindowHeight :: Int,
        pr_mainWindowX :: Int,
        pr_mainWindowY :: Int,
        pr_practiceWindowWidth :: Int,
        pr_practiceWindowHeight :: Int,
        pr_practiceWindowX :: Int,
        pr_practiceWindowY :: Int
    }

defaultPreferences :: Preferences
defaultPreferences = Preferences
    {
        pr_englishFonts = ["Arial 56"],
        pr_japaneseFonts = ["MS PMincho 56"],
        pr_mainWindowWidth = 1800,
        pr_mainWindowHeight = 1000,
        pr_mainWindowX = 100,
        pr_mainWindowY = 100,
        pr_practiceWindowWidth = 1100,
        pr_practiceWindowHeight = 600,
        pr_practiceWindowX = 2300,
        pr_practiceWindowY = 550
    }


getNamedChildNodeInt :: T.Text -> Element -> Int
getNamedChildNodeInt name node =
    case getNamedChildNodeTextM name node of
        Nothing -> 0
        Just textVal -> fromMaybe 0 (readMay (T.unpack textVal) :: Maybe Int)


loadPreferencesFile :: T.Text -> IO Preferences
loadPreferencesFile fileName = do
    Document _ root _ <- readFile def (T.unpack fileName)
    return $ parsePreferencesXML root


parsePreferencesXML :: Element -> Preferences
parsePreferencesXML root =
        Preferences
            {
                pr_englishFonts = englishFontTexts,
                pr_japaneseFonts = japaneseFontTexts,
                pr_mainWindowWidth = mainWindowWidth,
                pr_mainWindowHeight = mainWindowHeight,
                pr_mainWindowX = mainWindowX,
                pr_mainWindowY = mainWindowY,
                pr_practiceWindowWidth = practiceWindowWidth,
                pr_practiceWindowHeight = practiceWindowHeight,
                pr_practiceWindowX = practiceWindowX,
                pr_practiceWindowY = practiceWindowY
            }

    where
        englishFontNodesM = getNamedChildNodesM "englishFont" root
        japaneseFontNodesM = getNamedChildNodesM "japaneseFont" root

        englishFontNodes = fromMaybe [] englishFontNodesM
        japaneseFontNodes = fromMaybe [] japaneseFontNodesM

        englishFontTextsM = fmap getElementTextNodeM englishFontNodes
        japaneseFontTextsM = fmap getElementTextNodeM japaneseFontNodes

        englishFontTexts = catMaybes englishFontTextsM
        japaneseFontTexts = catMaybes japaneseFontTextsM

        mainWindowWidth = getNamedChildNodeInt "mainWindowWidth" root
        mainWindowHeight = getNamedChildNodeInt "mainWindowHeight" root
        practiceWindowWidth = getNamedChildNodeInt "practiceWindowWidth" root
        practiceWindowHeight = getNamedChildNodeInt "practiceWindowHeight" root
        mainWindowX = getNamedChildNodeInt "mainWindowX" root
        mainWindowY = getNamedChildNodeInt "mainWindowY" root
        practiceWindowX = getNamedChildNodeInt "practiceWindowX" root
        practiceWindowY = getNamedChildNodeInt "practiceWindowY" root


savePreferencesFile :: Preferences -> T.Text -> IO ()
savePreferencesFile preferences filename =
    DTIO.writeFile (T.unpack filename) (preferencesStr preferences)


preferencesStr :: Preferences -> L.Text
preferencesStr preferences =
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" `L.append`
        "<GtkParleyPreferences version=\"1.1\">\n" `L.append`
            englishFontsText `L.append`
            japaneseFontsText `L.append`
        "    <mainWindowWidth>" `L.append` mainWindowWidth `L.append` "</mainWindowWidth>\n" `L.append`
        "    <mainWindowHeight>" `L.append` mainWindowHeight `L.append` "</mainWindowHeight>\n" `L.append`
        "    <practiceWindowWidth>" `L.append` practiceWindowWidth `L.append` "</practiceWindowWidth>\n" `L.append`
        "    <practiceWindowHeight>" `L.append` practiceWindowHeight `L.append` "</practiceWindowHeight>\n" `L.append`
        "    <mainWindowX>" `L.append` mainWindowX `L.append` "</mainWindowX>\n" `L.append`
        "    <mainWindowY>" `L.append` mainWindowY `L.append` "</mainWindowY>\n" `L.append`
        "    <practiceWindowX>" `L.append` practiceWindowX `L.append` "</practiceWindowX>\n" `L.append`
        "    <practiceWindowY>" `L.append` practiceWindowY `L.append` "</practiceWindowY>\n" `L.append`
        "</GtkParleyPreferences>"
    where
        englishFontsText = fontsListText "englishFont" (pr_englishFonts preferences)
        japaneseFontsText = fontsListText "japaneseFont" (pr_japaneseFonts preferences)

        mainWindowWidth = L.pack . show $ pr_mainWindowWidth preferences
        mainWindowHeight = L.pack . show $ pr_mainWindowHeight preferences
        mainWindowX = L.pack . show $ pr_mainWindowX preferences
        mainWindowY = L.pack . show $ pr_mainWindowY preferences
        practiceWindowWidth = L.pack . show $ pr_practiceWindowWidth preferences
        practiceWindowHeight = L.pack . show $ pr_practiceWindowHeight preferences
        practiceWindowX = L.pack . show $ pr_practiceWindowX preferences
        practiceWindowY = L.pack . show $ pr_practiceWindowY preferences


fontsListText :: L.Text -> [T.Text] -> L.Text
fontsListText tagName fontList =
        L.concat fontListText
    where
        fontListText = fmap (\font -> "    <" `L.append` tagName `L.append` ">"
                                `L.append` L.fromStrict font `L.append`
                                "</" `L.append` tagName `L.append` ">\n"
                            ) fontList
